package com.epam.learn.odubelt.hospital.web.command.admin.user;

import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.AdminDao;
import com.epam.learn.odubelt.hospital.db.PatientDao;
import com.epam.learn.odubelt.hospital.db.PersonalDao;
import com.epam.learn.odubelt.hospital.db.entity.*;
import com.epam.learn.odubelt.hospital.web.command.Command;
import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.command.CommonCommands;
import com.epam.learn.odubelt.hospital.web.command.ValidationException;
import org.apache.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Command for adding new user.
 *
 * @author O.Dubelt
 *
 */
public class AddUserCommand extends Command {
    private static final long serialVersionUID = -3071536593627692473L;
    private static final Logger log = Logger.getLogger(AddUserCommand.class);
    public static final String PARAMETER_ADD_TYPE = "addType";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.debug("Command starts");
        String addType = request.getParameter(PARAMETER_ADD_TYPE);
        log.trace("Get the request attribute: addType --> " + addType);
        User user = createEmptyUserByType(addType);
        try {
            new UserUpdater().updateUserFromRequest(request, user);
        } catch (ValidationException e) {
            CommonCommands.setAttributesFromAllParameters(request);
            request.setAttribute(Parameters.ERROR_MESSAGE, e.getMessage());
            log.trace("Validation error --> " + e.getMessage());
            return Path.PAGE_FORM_USER;
        }
        //insert user to database
        insertUserToDB(user);

        log.debug("Command finished");
        return Path.COMMAND_SUCCESS;
    }

    /**
     * Creates empty user of given type
     * @param type from request
     * @return instance of user
     */
    private User createEmptyUserByType(String type) {
        if (Parameters.PATIENT.equals(type)){
            return new Patient();
        }
        if (Parameters.ADMIN.equals(type)){
            return new Admin();
        }
        Personal user = new Personal();
        if (Parameters.DOCTOR.equals(type)){
            user.setRoleId(1);
        }
        if (Parameters.NURSE.equals(type)){
            user.setRoleId(2);
        }
        return user;
    }
    /**
     * Inserts user to database
     * @param user to insert
     */
    private void insertUserToDB(User user) {
        if (user instanceof Admin) {
            new AdminDao().insertAdmin((Admin) user);
        }
        if (user instanceof Patient) {
            new PatientDao().insertPatient((Patient) user);
        }
        if (user instanceof Personal) {
            new PersonalDao().insertPersonal((Personal) user);
        }
        log.trace("Insert to database user --> " + user);
    }

}
