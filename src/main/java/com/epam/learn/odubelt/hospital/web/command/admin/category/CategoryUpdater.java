package com.epam.learn.odubelt.hospital.web.command.admin.category;

import com.epam.learn.odubelt.hospital.db.entity.*;
import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.Regex;
import com.epam.learn.odubelt.hospital.web.command.ValidationException;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Class for operating category objects
 */
public class CategoryUpdater {
    /**
     * Updates Category object by given request's parameters
     * @param request HttpServletRequest
     * @throws ValidationException if request parameters are not valid
     */
    public void updateCategoryFromRequest(HttpServletRequest request, Category category) throws ValidationException {

        String locale = ((User)request.getSession().getAttribute(Parameters.USER)).getLocale();
        ResourceBundle bundle = ResourceBundle.getBundle(Parameters.RESOURCES, Locale.forLanguageTag(locale));

        String nameUa = request.getParameter(Parameters.NAME_UA);
        if (nameUa != null && !nameUa.isEmpty()){
            if (nameUa.matches(Regex.UA_NAMES)) {
                category.setNameUa(nameUa);
            } else {
                throw new ValidationException(bundle.getString("regex.category.ua_name"));
            }
        }
        String nameEn = request.getParameter(Parameters.NAME_EN);
        if (nameEn != null && !nameEn.isEmpty()){
            if (nameEn.matches(Regex.EN_NAMES)) {
                category.setNameEn(nameEn);
            } else {
                throw new ValidationException(bundle.getString("regex.category.en_name"));
            }
        }
        String nameRu = request.getParameter(Parameters.NAME_RU);
        if (nameRu != null && !nameRu.isEmpty()){
            if (nameRu.matches(Regex.RU_NAMES)) {
                category.setNameRu(nameRu);
            } else {
                throw new ValidationException(bundle.getString("regex.category.ru_name"));
            }
        }
    }
}
