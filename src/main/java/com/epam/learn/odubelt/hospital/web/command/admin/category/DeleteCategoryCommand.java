package com.epam.learn.odubelt.hospital.web.command.admin.category;

import com.epam.learn.odubelt.hospital.db.SQLRestrictException;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.CategoryDao;
import com.epam.learn.odubelt.hospital.web.Parameters;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Command for deleting category.
 *
 * @author O.Dubelt
 *
 */
public class DeleteCategoryCommand extends com.epam.learn.odubelt.hospital.web.command.Command {
    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger log = Logger.getLogger(DeleteCategoryCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        log.debug("Command starts");

        Long id = Long.parseLong(request.getParameter(Parameters.ID));
        try {
            new CategoryDao().deleteCategory(id);
        } catch (SQLRestrictException ex) {
            request.setAttribute(Parameters.ERROR_MESSAGE, ex.getMessage());
            return Path.PAGE_ERROR_PAGE;
        }

        log.debug("Command finished");
        return Path.COMMAND_SUCCESS;
    }
}
