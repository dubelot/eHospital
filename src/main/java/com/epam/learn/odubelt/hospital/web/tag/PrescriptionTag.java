package com.epam.learn.odubelt.hospital.web.tag;

import com.epam.learn.odubelt.hospital.db.PrescriptionDao;
import com.epam.learn.odubelt.hospital.db.entity.Prescription;
import com.epam.learn.odubelt.hospital.web.Parameters;
import org.apache.log4j.Logger;
import javax.servlet.jsp.tagext.TagSupport;
/**
 * JSP Tag for getting prescription object by id
 *
 * @author O.Dubelt
 */
public class PrescriptionTag extends TagSupport {
    private static final long serialVersionUID = 6544856157948879418L;

    private String prescriptionId;
    private static final Logger log = Logger.getLogger(PrescriptionTag.class);
    public void setPrescriptionId (String prescriptionId) {
        this.prescriptionId = prescriptionId;
    }

    @Override
    public int doStartTag(){


        if (prescriptionId ==null || prescriptionId.isEmpty()) return 0;
        Long id = Long.parseLong(prescriptionId);
        Prescription prescription = new PrescriptionDao().findPrescription(id);
        pageContext.setAttribute(Parameters.PRESCRIPTION, prescription);


        return EVAL_BODY_INCLUDE;
    }

}
