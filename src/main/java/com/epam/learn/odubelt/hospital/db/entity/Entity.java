package com.epam.learn.odubelt.hospital.db.entity;

import javax.persistence.Id;
import java.io.Serializable;

/**
 * Root of all entities which have identifier field.
 * 
 * @author O.Dubelt
 * 
 */
@javax.persistence.Entity
public abstract class Entity implements Serializable {

	private static final long serialVersionUID = 2424210122260209492L;
	@Id
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
