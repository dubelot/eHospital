package com.epam.learn.odubelt.hospital.web.command.admin.user;

import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.CategoryDao;
import com.epam.learn.odubelt.hospital.db.PersonalDao;
import com.epam.learn.odubelt.hospital.db.entity.Category;
import com.epam.learn.odubelt.hospital.db.entity.Personal;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
/**
 * Command for open form for adding new user.
 *
 * @author O.Dubelt
 *
 */
public class OpenUserFormCommand extends com.epam.learn.odubelt.hospital.web.command.Command {
    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger log = Logger.getLogger(OpenUserFormCommand.class);
    public static final String ADD_TYPE = "addType";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.debug("Command starts");

        List<Category> categoryList = new CategoryDao().getCategoryList();
        log.trace("Found in DB: categoryList --> " + categoryList);
        // put category list to request
        request.setAttribute(Parameters.CATEGORY_LIST, categoryList);
        log.trace("Set the request attribute: categoryList --> " + categoryList);

        String addType = request.getParameter(ADD_TYPE);
        request.setAttribute(ADD_TYPE, addType);
        log.trace("Set the request attribute: addType --> " + addType);

        if (Parameters.DOCTOR.equals(addType)) {
            List<Personal> nurseList = new PersonalDao().getNurseList();
            log.trace("Found in DB: nurseList --> " + nurseList);
            request.setAttribute(Parameters.NURSE_LIST, nurseList);
            log.trace("Set the request attribute: nurseList --> " + nurseList);
        }
        log.debug("Command finished");
        return Path.PAGE_FORM_USER;
    }
}
