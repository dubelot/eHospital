package com.epam.learn.odubelt.hospital.web.command.admin;

import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.PatientDao;
import com.epam.learn.odubelt.hospital.db.PatientHasDoctorDao;
import com.epam.learn.odubelt.hospital.db.entity.Admin;
import com.epam.learn.odubelt.hospital.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Command for appointing doctor to a patient.
 *
 * @author O.Dubelt
 *
 */
public class AppointDoctorCommand extends Command {
    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger log = Logger.getLogger(AppointDoctorCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.debug("Command starts");

        long patientId = Long.parseLong(request.getParameter(Parameters.PATIENT_ID));
        long personalId = Long.parseLong(request.getParameter(Parameters.PERSONAL_ID));
        Admin admin = (Admin) request.getSession().getAttribute(Parameters.USER);
        Long adminId = null;
        if (admin!=null) {
            adminId = admin.getId();
        }
        if(patientId!=0 && personalId!=0 && adminId!=null){
            new PatientHasDoctorDao().insertEntry(patientId, personalId, adminId);
        }
        new PatientDao().updatePatientApplication(patientId, null);

        log.debug("Command finished");
        return Path.COMMAND_SUCCESS;
    }
}
