package com.epam.learn.odubelt.hospital.web.command.admin.category;

import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.CategoryDao;
import com.epam.learn.odubelt.hospital.db.entity.Category;
import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Command for open form for editing category.
 *
 * @author O.Dubelt
 *
 */
public class OpenEditCategoryFormCommand extends Command {
    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger log = Logger.getLogger(OpenEditCategoryFormCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.debug("Command starts");

        Long id = Long.parseLong(request.getParameter(Parameters.ID));
        Category category = new CategoryDao().findCategory(id);
        request.setAttribute(Parameters.CATEGORY, category);
        log.trace("Set the request attribute: category --> " + category);

        log.debug("Command finished");
        return Path.PAGE_FORM_EDIT_CATEGORY;
    }
}
