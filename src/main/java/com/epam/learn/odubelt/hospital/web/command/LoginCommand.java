package com.epam.learn.odubelt.hospital.web.command;

import com.epam.learn.odubelt.hospital.web.EHospUtility;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.AdminDao;
import com.epam.learn.odubelt.hospital.db.PatientDao;
import com.epam.learn.odubelt.hospital.db.PersonalDao;
import com.epam.learn.odubelt.hospital.db.entity.Admin;
import com.epam.learn.odubelt.hospital.db.entity.Patient;
import com.epam.learn.odubelt.hospital.db.entity.Personal;
import com.epam.learn.odubelt.hospital.db.entity.User;
import com.epam.learn.odubelt.hospital.web.Parameters;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Login command.
 * 
 * @author O.Dubelt
 * 
 */
public class LoginCommand extends Command {

	private static final long serialVersionUID = 4746672079608598998L;
	private static final Logger log = Logger.getLogger(LoginCommand.class);

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		log.debug("Command starts");
		
		HttpSession session = request.getSession();
		String locale = (String) request.getSession().getAttribute(Parameters.DEFAULT_LOCALE);
		if (locale==null) locale = Parameters.UA;
		ResourceBundle bundle = ResourceBundle.getBundle(Parameters.RESOURCES, Locale.forLanguageTag(locale));
		// obtain login and password from the request
		String login = request.getParameter(Parameters.LOGIN);
		log.trace("Request parameter: login --> " + login);
		String password = request.getParameter(Parameters.PASSWORD);
		
		User user = new AdminDao().findAdminByLogin(login);
		if (user == null) user = new PersonalDao().findPersonalByLogin(login);
		if (user == null) user = new PatientDao().findPatientByLogin(login);
		log.trace("Found in DB: user --> " + user);

		if (user == null || !EHospUtility.comparePasswords(password, user.getPassword())) {
			String errorMessage = bundle.getString("user.illegal_login_password");
			request.setAttribute(Parameters.ERROR_MESSAGE, errorMessage);
			log.error("errorMessage --> " + errorMessage);
			return Path.PAGE_ERROR_PAGE;
		}
		String userType	= user.getClass().getSimpleName().toLowerCase();
		session.setAttribute(Parameters.USER, user);
		log.trace("Set the session attribute: user --> " + user);
		session.setAttribute(Parameters.USER_TYPE, userType);
		log.trace("Set the session attribute: userType --> " + userType);
		log.info("User " + user + " logged as " + userType);
		// work with i18n
		String userLocale = user.getLocale();
		log.trace("userLocalName --> " + userLocale);
		if (userLocale != null && !userLocale.isEmpty()) {
			Config.set(session, Parameters.JAVAX_SERVLET_JSP_JSTL_FMT_LOCALE, userLocale);
			session.setAttribute(Parameters.DEFAULT_LOCALE, userLocale);
			log.trace("Set the session attribute: defaultLocale --> " + userLocale);
			log.info("Locale for user: defaultLocale --> " + userLocale);
		}
		String forward = null;
		if (user instanceof Admin) {
			forward = Path.COMMAND_LIST_PERSONAL;
		}
		if (user instanceof Patient) {
			forward = Path.COMMAND_PATIENT_DEFAULT;
		}
		if (user instanceof Personal) {
			forward = Path.COMMAND_PERSONAL_DEFAULT;
		}
		log.debug("Command finished");
		return forward;
	}
}