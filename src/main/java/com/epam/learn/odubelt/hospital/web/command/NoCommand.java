package com.epam.learn.odubelt.hospital.web.command;

import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.web.Parameters;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * No command.
 * 
 * @author O.Dubelt
 * 
 */
public class NoCommand extends Command {

	private static final long serialVersionUID = -2785976616686657267L;

	private static final Logger log = Logger.getLogger(NoCommand.class);

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		log.debug("Command starts");
		String locale = (String) request.getSession().getAttribute(Parameters.DEFAULT_LOCALE);
		ResourceBundle bundle = ResourceBundle.getBundle(Parameters.RESOURCES, Locale.forLanguageTag(locale));
		String errorMessage = bundle.getString("no_such_command");
		request.setAttribute(Parameters.ERROR_MESSAGE, errorMessage);
		log.error("Set the request attribute: errorMessage --> " + errorMessage);

		log.debug("Command finished");
		return Path.PAGE_ERROR_PAGE;
	}

}