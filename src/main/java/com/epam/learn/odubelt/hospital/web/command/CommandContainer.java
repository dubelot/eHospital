package com.epam.learn.odubelt.hospital.web.command;

import com.epam.learn.odubelt.hospital.web.command.admin.*;
import com.epam.learn.odubelt.hospital.web.command.admin.category.*;
import com.epam.learn.odubelt.hospital.web.command.admin.user.*;
import com.epam.learn.odubelt.hospital.web.command.patient.PatientDefaultCommand;
import com.epam.learn.odubelt.hospital.web.command.patient.SendApplicationCommand;
import com.epam.learn.odubelt.hospital.web.command.personal.AddCardEntryCommand;
import com.epam.learn.odubelt.hospital.web.command.personal.OpenEntryFormCommand;
import com.epam.learn.odubelt.hospital.web.command.personal.PersonalDefaultCommand;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.TreeMap;

/**
 * Holder for all commands.
 * 
 * @author O.Dubelt
 * 
 */
public class CommandContainer {
	
	private static final Logger log = Logger.getLogger(CommandContainer.class);
	
	private static final Map<String, Command> commands = new TreeMap<>();
	
	static {
		// common commands
		commands.put("login", new LoginCommand());
		commands.put("logout", new LogoutCommand());
		commands.put("noCommand", new NoCommand());
		commands.put("localeSet", new LocaleSetCommand());
		commands.put("viewSettings", new ViewSettingsCommand());
		commands.put("updateSettings", new UpdateSettingsCommand());
		commands.put("viewMedCard", new ViewMedCardCommand());
		commands.put("savePDF", new SavePDFCommand());
		commands.put("success", new SuccessCommand());
		
		// admin commands
		commands.put("listPersonal", new ListPersonalCommand());
		commands.put("listCategory", new ListCategoryCommand());
		commands.put("listAdmin", new ListAdminCommand());
		commands.put("listPatient", new ListPatientCommand());
		commands.put("addUserForm", new OpenUserFormCommand());
		commands.put("openUserForm", new OpenUserFormCommand());
		commands.put("openEditUserForm", new OpenEditUserFormCommand());
		commands.put("addUser", new AddUserCommand());
		commands.put("editUser", new EditUserCommand());
		commands.put("delUser", new DeleteUserCommand());
		commands.put("openCategoryForm", new OpenCategoryFormCommand());
		commands.put("openEditCategoryForm", new OpenEditCategoryFormCommand());
		commands.put("addCategory", new AddCategoryCommand());
		commands.put("editCategory", new EditCategoryCommand());
		commands.put("delCategory", new DeleteCategoryCommand());
		commands.put("formAppointDoctor", new FormAppointDoctorCommand());
		commands.put("appointDoctor", new AppointDoctorCommand());
		commands.put("detachDoctor", new DetachDoctorCommand());
		commands.put("detachNurse", new DetachNurseCommand());

		// patient commands
		commands.put("patientDefault", new PatientDefaultCommand());
		commands.put("sendApplication", new SendApplicationCommand());
		// personal commands
		commands.put("personalDefault", new PersonalDefaultCommand());
		commands.put("openEntryForm", new OpenEntryFormCommand());
		commands.put("addCardEntry", new AddCardEntryCommand());

		log.debug("Command container was successfully initialized");
		log.trace("Number of commands --> " + commands.size());
	}

	/**
	 * Returns command object with the given name.
	 * 
	 * @param commandName
	 *            Name of the command.
	 * @return Command object.
	 */
	public static Command get(String commandName) {
		if (commandName == null || !commands.containsKey(commandName)) {
			log.trace("Command not found, name --> " + commandName);
			return commands.get("noCommand"); 
		}
		return commands.get(commandName);
	}
	
}