package com.epam.learn.odubelt.hospital.web.command;

/**
 * Class for exceptions if entity field from user input form is not valid
 */
public class ValidationException extends Exception{
    private static final long serialVersionUID = -8971870440810220404L;

    public ValidationException(String message) {
        super(message);
    }
}
