package com.epam.learn.odubelt.hospital.web;
import org.apache.commons.codec.DecoderException;
import org.apache.log4j.Logger;
import org.apache.commons.codec.binary.Hex;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

/**
 * Utility for password hashing and other common methods.
 *
 * @author O.Dubelt
 *
 */
public class EHospUtility {
    private static final Logger log = Logger.getLogger(EHospUtility.class);

    private EHospUtility(){
    }
    /**
     * Password hashing with random salt for storing it in database.
     * @param password input password by user
     * @return password for storing it in database
     */
    public static String hashPassword(String password) {
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[16];
        random.nextBytes(salt);
        return hashPasswordWithSalt(password, salt);
    }

    private static String hashPasswordWithSalt(String password, byte[] salt) {
        String result = null;
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] hash = factory.generateSecret(spec).getEncoded();
            result = Hex.encodeHexString(hash) + Hex.encodeHexString(salt);
        } catch (NoSuchAlgorithmException|InvalidKeySpecException ex){
            log.error(ex.getMessage());
        }
        return result;
    }
    /**
     * Password hashing with random salt for storing it in database.
     * @param inputPassword input password by user
     * @param storedPassword password from database
     * @return true if passwords without salt are equal
     */
    public static boolean comparePasswords (String inputPassword, String storedPassword) {
        if (inputPassword==null) return false;
        String salt = storedPassword.substring(32);
        try {
            return storedPassword.equals(hashPasswordWithSalt(inputPassword, Hex.decodeHex(salt.toCharArray())));
        } catch (DecoderException e) {
            e.printStackTrace();
        }
        return false;
    }

}
