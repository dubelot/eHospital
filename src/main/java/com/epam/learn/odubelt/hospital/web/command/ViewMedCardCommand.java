package com.epam.learn.odubelt.hospital.web.command;

import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.MedCardEntriesDAO;
import com.epam.learn.odubelt.hospital.db.PatientDao;
import com.epam.learn.odubelt.hospital.db.entity.CardEntry;
import com.epam.learn.odubelt.hospital.db.entity.Patient;
import com.epam.learn.odubelt.hospital.db.entity.User;
import com.epam.learn.odubelt.hospital.web.Parameters;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
/**
 * View medical card command. Returns error page if patient tries to look at another patient's medical card.
 *
 * @author O.Dubelt
 *
 */
public class ViewMedCardCommand extends Command {
    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger log = Logger.getLogger(ViewMedCardCommand.class);
    public static final int PAGE_CAPACITY = 3;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.debug("Command starts");
        Long id = Long.parseLong(request.getParameter(Parameters.ID));
        
        //restricting access to other patient's medical cards
        User user = (User) request.getSession().getAttribute(Parameters.USER);
        if (user instanceof Patient) {
            if (!((Patient) user).getMedCardId().equals(id)) {
                request.setAttribute(Parameters.ERROR_MESSAGE, "Access error");
                return Path.PAGE_ERROR_PAGE;
            }
        }
        int size = new MedCardEntriesDAO().listSize(id);
        log.trace("Found in DB: size --> " + size);

        int currentPage = CommonCommands.setPaginationParameters(request, size, PAGE_CAPACITY);

        boolean desc = Boolean.parseBoolean(request.getParameter(Parameters.DESC));
        request.setAttribute(Parameters.DESC, desc);
        log.trace("Set the request attribute: desc --> " + desc);

        ArrayList <CardEntry> entryList = new MedCardEntriesDAO().getEntryList(id, currentPage, desc);
        request.setAttribute(Parameters.ENTRY_LIST, entryList);
        log.trace("Set the request attribute: entryList --> " + entryList);

        Patient patient = new PatientDao().findPatientByMedCardId(id);
        request.setAttribute(Parameters.PATIENT, patient);
        log.trace("Set the request attribute: patient --> " + patient);
        log.debug("Command finished");
        return Path.PAGE_MED_CARD;
    }
}
