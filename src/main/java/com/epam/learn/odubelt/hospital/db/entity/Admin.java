package com.epam.learn.odubelt.hospital.db.entity;

import javax.persistence.Entity;

/**
 * Admin entity.
 * 
 * @author O.Dubelt
 * 
 */
@Entity
public class Admin extends User {

	private static final long serialVersionUID = 5694732653327033075L;

	@Override
	public String toString() {
		return "Admin [id=" + getId()+ ", login=" + getLogin() + ", password=" + getPassword()
				+ ", firstName=" + getFirstName() + ", lastName=" + getLastName()
				+ ", locale=" + getLocale() + ']';
	}

}
