package com.epam.learn.odubelt.hospital.web.command.admin;

import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.AdminDao;
import com.epam.learn.odubelt.hospital.db.entity.Admin;
import com.epam.learn.odubelt.hospital.web.command.Command;
import com.epam.learn.odubelt.hospital.web.command.CommonCommands;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
/**
 * Admin list.
 *
 * @author O.Dubelt
 *
 */
public class ListAdminCommand extends Command {

    private static final long serialVersionUID = -4583733129364537773L;
    private static final Logger log = Logger.getLogger(ListAdminCommand.class);
    public static final int PAGE_CAPACITY = 10;

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) throws IOException, ServletException {
        log.debug("Commands starts");

        int size = new AdminDao().listSize();
        int currentPage = CommonCommands.setPaginationParameters(request, size, PAGE_CAPACITY);
        log.trace("Set the request attribute: currentPage --> " + currentPage);

        boolean desc = Boolean.parseBoolean(request.getParameter(Parameters.DESC));
        request.setAttribute(Parameters.DESC, desc);
        log.trace("Set the request attribute: desc --> " + desc);

        List<Admin> adminList = new AdminDao().getAdminList(currentPage, desc);
        log.trace("Found in DB: adminList --> " + adminList);
        request.setAttribute(Parameters.ADMIN_LIST, adminList);
        log.trace("Set the request attribute: adminList --> " + adminList);

        log.debug("Commands finished");
        return Path.PAGE_LIST_ADMIN;
    }
}
