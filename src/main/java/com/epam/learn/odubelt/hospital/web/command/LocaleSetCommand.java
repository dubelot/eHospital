package com.epam.learn.odubelt.hospital.web.command;

import com.epam.learn.odubelt.hospital.web.Parameters;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;
/**
 * Command for setting locale on login page.
 *
 * @author O.Dubelt
 *
 */
public class LocaleSetCommand extends Command {

    private static final long serialVersionUID = 7732286214029478505L;
    private static final Logger log = Logger.getLogger(LocaleSetCommand.class);

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) throws IOException, ServletException {

        log.debug("Command starts");

        String localeToSet = request.getParameter(Parameters.LOCALE_TO_SET);
        if (localeToSet != null && !localeToSet.isEmpty()) {
            HttpSession session = request.getSession();
            Config.set(session, Parameters.JAVAX_SERVLET_JSP_JSTL_FMT_LOCALE, localeToSet);
            session.setAttribute(Parameters.DEFAULT_LOCALE, localeToSet);
        }
        log.debug("Command finished");
        return "";
    }
}
