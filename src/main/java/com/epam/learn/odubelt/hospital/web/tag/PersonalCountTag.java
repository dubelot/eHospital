package com.epam.learn.odubelt.hospital.web.tag;

import com.epam.learn.odubelt.hospital.db.entity.Personal;
import com.epam.learn.odubelt.hospital.web.Parameters;
import org.apache.log4j.Logger;
import javax.servlet.jsp.tagext.TagSupport;
import java.util.ArrayList;
/**
 * JSP Tag for getting amount of personal by current category
 *
 * @author O.Dubelt
 */
public class PersonalCountTag extends TagSupport {
    private static final long serialVersionUID = 6544856157948879418L;
    private String categoryId;
    private static final Logger log = Logger.getLogger(PersonalCountTag.class);

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public int doStartTag() {


        ArrayList<Personal> list = (ArrayList<Personal>) pageContext.getRequest().getAttribute(Parameters.PERSONAL_LIST);
        int id = Integer.parseInt(categoryId);
        int count = 0;
        for (Personal personal: list) {
            if (personal.getCategoryId()==id) count++;
        }
        pageContext.setAttribute(Parameters.COUNT_PERSONAL, count);


        return EVAL_BODY_INCLUDE;
    }

}
