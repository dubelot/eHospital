package com.epam.learn.odubelt.hospital.db;

/**
 * Holder for fields names of DB tables.
 * 
 * @author O.Dubelt
 * 
 */
public final class Fields {
	
	// entities
	public static final String ENTITY_ID = "id";

	public static final String USER_LOGIN = "login";
	public static final String USER_PASSWORD = "password";
	public static final String USER_FIRST_NAME = "first_name";
	public static final String USER_LAST_NAME = "last_name";
	public static final String USER_ID_NUMBER = "id_number";
	public static final String USER_LOCALE = "locale";
	public static final String USER_ACTIVE = "active";

	public static final String PERSONAL_CATEGORY_ID = "category_id";
	public static final String PERSONAL_ROLE_ID = "personal_role_id";
	public static final String PERSONAL_NURSE_ID = "nurse_id";
	public static final String PERSONAL_COUNT = "count";

	public static final String CATEGORY_NAME_UA = "name_ua";
	public static final String CATEGORY_NAME_EN = "name_en";
	public static final String CATEGORY_NAME_RU = "name_ru";

	public static final String PATIENT_BIRTHDAY = "birthday";
	public static final String PATIENT_MED_CARD_ID = "med_card_id";
	public static final String PATIENT_APPLICATION = "application";

	public static final String ENTRY_CREATE_TIME = "create_time";
	public static final String ENTRY_PERSONAL_ID = "personal_id";
	public static final String ENTRY_ENTRY_TYPE_ID = "entry_type_id";
	public static final String ENTRY_SYMPTOMS = "symptoms";
	public static final String ENTRY_NOTE = "note";
	public static final String ENTRY_DIAGNOSIS = "diagnosis";
	public static final String ENTRY_PRESCRIPTION_ID = "prescription_id";
	public static final String ENTRY_MED_CARD_ID = "med_card_id";

	public static final String PRESCRIPTION_MEDICINE = "medicine";
	public static final String PRESCRIPTION_PROCEDURE = "procedure";
	public static final String PRESCRIPTION_SURGERY = "surgery";

}