package com.epam.learn.odubelt.hospital.db;

import com.epam.learn.odubelt.hospital.db.entity.Admin;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Data access object for Admin entity.
 */
public class AdminDao {
    private static final String SQL_LIST_ADMIN_SIZE =
            "SELECT COUNT(id) FROM admin";
    private static final String SQL_FIND_ADMIN_LIST =
            "SELECT * FROM admin ORDER BY last_name, first_name LIMIT ?, ?";
    private static final String SQL_FIND_ADMIN_BY_LOGIN =
            "SELECT * FROM admin WHERE login=?";
    private static final String SQL_FIND_ADMIN_BY_ID =
            "SELECT * FROM admin WHERE id=?";
    private static final String SQL_INSERT_ADMIN =
            "INSERT INTO admin (login, password, first_name, last_name, id_number) VALUES(?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE_ADMIN =
            "UPDATE admin SET password=?, first_name=?, last_name=?, id_number=?, locale=?, active=?"+
                    "	WHERE id=?";
    private static final String SQL_DELETE_ADMIN = "DELETE FROM admin WHERE id=?";

    /**
     * Counts all admins in database
     * @return size of admin table
     */
    public int listSize() {
        Statement stmt;
        ResultSet rs;
        Connection con = null;
        int size = 0;
        try {
            con = DBManager.getInstance().getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL_LIST_ADMIN_SIZE);
            rs.next();
            size = rs.getInt(1);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
                DBManager.getInstance().commitAndClose(con);
        }
        return size;
    }
    /**
     * Returns an admin list for current page.
     *
     * @param currentPage
     *            number of page.
     * @param desc order of list.
     * @return list of admins.
     */
    public List<Admin> getAdminList(int currentPage, boolean desc) {
        ArrayList<Admin> list = new ArrayList<>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            String sql = SQL_FIND_ADMIN_LIST;
            StringBuffer sbSql;
            if (desc) {
                sbSql = new StringBuffer(sql);
                String order = " DESC";
                int offset = sbSql.lastIndexOf(Fields.USER_LAST_NAME) + Fields.USER_LAST_NAME.length();
                sbSql.insert(offset, order);
                offset = sbSql.lastIndexOf(Fields.USER_FIRST_NAME) + Fields.USER_FIRST_NAME.length();
                sbSql.insert(offset, order);
                sql = sbSql.toString();
            }
            con = DBManager.getInstance().getConnection();
            AdminMapper mapper = new AdminMapper();
            pstmt = con.prepareStatement(sql);
            int k = 1;
            pstmt.setInt(k++, (currentPage - 1) * 10);
            pstmt.setInt(k, currentPage * 10);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(mapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
        return list;
    }
    /**
     * Returns an admin with the given identifier.
     *
     * @param id Admin identifier.
     * @return Admin entity.
     */
    public Admin findAdmin(Long id) {
        Admin admin = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            AdminMapper mapper = new AdminMapper();
            pstmt = con.prepareStatement(SQL_FIND_ADMIN_BY_ID);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next())
                admin = mapper.mapRow(rs);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
        return admin;
    }

    /**
     * Returns an admin with the given login.
     *
     * @param login Admin login.
     * @return Admin entity.
     */
    public Admin findAdminByLogin(String login) {
        Admin admin = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            AdminMapper mapper = new AdminMapper();
            pstmt = con.prepareStatement(SQL_FIND_ADMIN_BY_LOGIN);
            pstmt.setString(1, login);
            rs = pstmt.executeQuery();
            if (rs.next())
                admin = mapper.mapRow(rs);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
        return admin;
    }
    /**
     * Insert new admin.
     *
     * @param admin to insert.
     */
    public void insertAdmin (Admin admin) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            insertAdmin(con, admin);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
    }
    /**
     * Insert new admin.
     *
     * @param admin to insert.
     * @throws SQLException if insert not valid data
     */
    public void insertAdmin(Connection con, Admin admin) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL_INSERT_ADMIN);
        int k = 1;
        pstmt.setString(k++, admin.getLogin());
        pstmt.setString(k++, admin.getPassword());
        pstmt.setString(k++, admin.getFirstName());
        pstmt.setString(k++, admin.getLastName());
        pstmt.setString(k, admin.getIdNumber());
        pstmt.executeUpdate();
        pstmt.close();
    }

    /**
     * Update the admin.
     *
     * @param admin to update.
     */
    public void updateAdmin(Admin admin) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            updateAdmin(con, admin);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
    }
    /**
     * Update the admin
     *
     * @param admin  to update.
     * @throws SQLException if insert not valid data
     */
    private void updateAdmin(Connection con, Admin admin) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL_UPDATE_ADMIN);
        int k = 1;
        pstmt.setString(k++, admin.getPassword());
        pstmt.setString(k++, admin.getFirstName());
        pstmt.setString(k++, admin.getLastName());
        pstmt.setString(k++, admin.getIdNumber());
        pstmt.setString(k++, admin.getLocale());
        pstmt.setInt(k++, admin.getActive());
        pstmt.setLong(k, admin.getId());
        pstmt.executeUpdate();
        pstmt.close();
    }
    /**
     * Deletes the admin.
     *
     * @param id  admin id to delete.
     * @exception SQLRestrictException if database restrict deleting an administrator who prescribed doctors to patients
     */
    public void deleteAdmin(Long id) throws SQLRestrictException {
        PreparedStatement pstmt;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_DELETE_ADMIN);
            pstmt.setLong(1, id);
            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            throw new SQLRestrictException("It is not possible to remove the administrator who prescribed doctors to patients", ex);
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
    }
    /**
     * Extracts an admin from the result set row.
     */
    private static class AdminMapper implements EntityMapper<Admin> {
        @Override
        public Admin mapRow(ResultSet rs) {
            try {
                Admin admin = new Admin();
                admin.setId(rs.getLong(Fields.ENTITY_ID));
                admin.setLogin(rs.getString(Fields.USER_LOGIN));
                admin.setPassword(rs.getString(Fields.USER_PASSWORD));
                admin.setFirstName(rs.getString(Fields.USER_FIRST_NAME));
                admin.setLastName(rs.getString(Fields.USER_LAST_NAME));
                admin.setIdNumber(rs.getString(Fields.USER_ID_NUMBER));
                admin.setLocale(rs.getString(Fields.USER_LOCALE));
                admin.setActive(rs.getInt(Fields.USER_ACTIVE));
                return admin;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
