package com.epam.learn.odubelt.hospital.web.command.admin.user;

import com.epam.learn.odubelt.hospital.db.SQLRestrictException;
import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Command for deleting user.
 *
 * @author O.Dubelt
 *
 */
public class DeleteUserCommand extends Command {
    private static final long serialVersionUID = -3071536593627692473L;
    public static final String PARAMETER_DEL_TYPE = "delType";
    private static final Logger log = Logger.getLogger(DeleteUserCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.debug("Command starts");

        String delType = request.getParameter(PARAMETER_DEL_TYPE);
        log.trace("Get the request attribute: delType --> " + delType);
        Long id = Long.parseLong(request.getParameter(Parameters.ID));
        try {
            new UserUpdater().deleteUserFromDB(delType, id);
        } catch (SQLRestrictException ex) {
            request.setAttribute(Parameters.ERROR_MESSAGE, ex.getMessage());
            return Path.PAGE_ERROR_PAGE;
        }
        log.debug("Command finished");

        return Path.COMMAND_SUCCESS;
    }
}

