package com.epam.learn.odubelt.hospital.web.command.admin;

import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.PersonalDao;
import com.epam.learn.odubelt.hospital.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Command for detaching nurse for a doctor.
 *
 * @author O.Dubelt
 *
 */
public class DetachNurseCommand extends Command {
    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger log = Logger.getLogger(DetachNurseCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.debug("Command starts");

        long id = Long.parseLong(request.getParameter(Parameters.ID));
        if(id!=0){
            new PersonalDao().detachNurse(id);
        }
        log.debug("Command finished");
        return Path.COMMAND_SUCCESS;
    }
}
