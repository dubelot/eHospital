package com.epam.learn.odubelt.hospital.db.entity;

/**
 * Prescription entity.
 * 
 * @author O.Dubelt
 * 
 */
@javax.persistence.Entity
public class Prescription extends Entity {

	private static final long serialVersionUID = -6009541873969677773L;

	private String medicine;
	private String procedure;
	private String surgery;

	public String getMedicine() {
		return medicine;
	}

	public void setMedicine(String medicine) {
		this.medicine = medicine;
	}

	public String getProcedure() {
		return procedure;
	}

	public void setProcedure(String procedure) {
		this.procedure = procedure;
	}

	public String getSurgery() {
		return surgery;
	}

	public void setSurgery(String surgery) {
		this.surgery = surgery;
	}

	@Override
	public String toString() {
		return "Prescription [id=" + getId() + ", medicine='" + medicine + '\'' +
				", procedure='" + procedure + '\'' +
				", surgery='" + surgery + '\'' +
				']';
	}
}
