package com.epam.learn.odubelt.hospital.web.command;

import com.epam.learn.odubelt.hospital.web.Path;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Command for success message after executing another command which changes data in database.
 *
 * @author O.Dubelt
 *
 */
public class SuccessCommand extends Command {
    private static final long serialVersionUID = 4746672079608598998L;

    private static final Logger log = Logger.getLogger(SuccessCommand.class);

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) throws IOException, ServletException {

        log.debug("Command starts");

        log.debug("Command finished");

        return Path.PAGE_SUCCESS;
    }
}
