package com.epam.learn.odubelt.hospital.web.command.admin.category;

import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.CategoryDao;
import com.epam.learn.odubelt.hospital.db.PersonalDao;
import com.epam.learn.odubelt.hospital.db.entity.Category;
import com.epam.learn.odubelt.hospital.db.entity.Entity;
import com.epam.learn.odubelt.hospital.db.entity.Personal;
import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.command.Command;
import com.epam.learn.odubelt.hospital.web.command.CommonCommands;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;

/**
 * Category list.
 * 
 * @author O.Dubelt
 * 
 */
public class ListCategoryCommand extends Command {

	private static final long serialVersionUID = -4583733129364537773L;
	private static final Logger log = Logger.getLogger(ListCategoryCommand.class);
	private static final int PAGE_CAPACITY = 10;
	private static final Comparator<Entity> compareById = Comparator.comparing(Entity::getId);

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		log.debug("Commands starts");

		List<Personal> personalList = new PersonalDao().getPersonalList();
		personalList.sort(compareById);
		log.trace("Found in DB: personalList --> " + personalList);
		request.setAttribute(Parameters.PERSONAL_LIST, personalList);
		log.trace("Set the request attribute: personal --> " + personalList);

		int size = new CategoryDao().listSize();
		log.trace("Found in DB: size --> " + size);
		int currentPage = CommonCommands.setPaginationParameters(request, size, PAGE_CAPACITY);
		log.trace("Set the request attribute: currentPage --> " + currentPage);

		Object paramLocale = request.getSession().getAttribute(Parameters.DEFAULT_LOCALE);
		String locale = null;
		if (paramLocale instanceof String) {
			locale = (String) paramLocale;
		}
		boolean desc = Boolean.parseBoolean(request.getParameter(Parameters.DESC));
		request.setAttribute(Parameters.DESC, desc);
		log.trace("Set the request attribute: desc --> " + desc);

		List<Category> categoryList = new CategoryDao().getCategoryList(currentPage, locale, desc);
		log.trace("Found in DB: categoryList --> " + categoryList);
		request.setAttribute(Parameters.CATEGORY_LIST, categoryList);
		log.trace("Set the request attribute: categoryList --> " + categoryList);

		log.debug("Commands finished");
		return Path.PAGE_LIST_CATEGORY;
	}

}