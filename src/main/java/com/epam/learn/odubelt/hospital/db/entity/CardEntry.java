package com.epam.learn.odubelt.hospital.db.entity;

import java.sql.Date;

/**
 * CardEntry entity.
 * 
 * @author O.Dubelt
 * 
 */
@javax.persistence.Entity
public class CardEntry extends Entity {

	private static final long serialVersionUID = 4821278231441088866L;

	private Date createTime;

	private Long personalId;
	private int entryTypeId;
	private Long medCardId;
	private String symptoms;
	private String note;
	private String diagnosis;
	private Long prescriptionId;

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getPersonalId() {
		return personalId;
	}

	public void setPersonalId(Long personalId) {
		this.personalId = personalId;
	}

	public int getEntryTypeId() {
		return entryTypeId;
	}

	public void setEntryTypeId(int entryTypeId) {
		this.entryTypeId = entryTypeId;
	}

	public Long getMedCardId() {
		return medCardId;
	}

	public void setMedCardId(Long medCardId) {
		this.medCardId = medCardId;
	}

	public String getSymptoms() {
		return symptoms;
	}

	public void setSymptoms(String symptoms) {
		this.symptoms = symptoms;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getDiagnosis() {
		return diagnosis;
	}

	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}

	public Long getPrescriptionId() {
		return prescriptionId;
	}

	public void setPrescriptionId(Long prescriptionId) {
		this.prescriptionId = prescriptionId;
	}

	@Override
	public String toString() {
		return "CardEntry [createTime=" + createTime +
				", personalId=" + personalId +
				", recordTypeId=" + entryTypeId +
				", medCardId=" + medCardId +
				", symptoms='" + symptoms + '\'' +
				", note='" + note + '\'' +
				", diagnosis='" + diagnosis + '\'' +
				", prescriptionId=" + prescriptionId +
				']';
	}
}