package com.epam.learn.odubelt.hospital.web.command.admin;

import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.PatientDao;
import com.epam.learn.odubelt.hospital.db.entity.Patient;
import com.epam.learn.odubelt.hospital.web.command.Command;
import com.epam.learn.odubelt.hospital.web.command.CommonCommands;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
/**
 * Patient list.
 *
 * @author O.Dubelt
 *
 */
public class ListPatientCommand extends Command {
    private static final long serialVersionUID = 1863978254689586513L;
    private static final Logger log = Logger.getLogger(ListPatientCommand.class);
    private static final int PAGE_CAPACITY = 10;


    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) throws IOException, ServletException {


        log.debug("Commands starts");

        int size = new PatientDao().listSize();
        int currentPage = CommonCommands.setPaginationParameters(request, size, PAGE_CAPACITY);
        log.trace("Set the request attribute: currentPage --> " + currentPage);

        boolean desc = Boolean.parseBoolean(request.getParameter(Parameters.DESC));
        request.setAttribute(Parameters.DESC, desc);
        log.trace("Set the request attribute: desc --> " + desc);

        String orderBy = request.getParameter(Parameters.ORDER_BY);
        log.trace("Request parameter: orderBy --> " + orderBy);
        request.setAttribute(Parameters.ORDER_BY, orderBy);
        log.trace("Set the request attribute: orderBy --> " + orderBy);

        List<Patient> patientList = new PatientDao().getPatientList(currentPage, orderBy, desc);
        log.trace("Found in DB: patientList --> " + patientList);

        // put personal list to request
        request.setAttribute(Parameters.PATIENT_LIST, patientList);
        log.trace("Set the request attribute: patientList --> " + patientList);

        log.debug("Commands finished");
        return Path.PAGE_LIST_PATIENT;
    }
}
