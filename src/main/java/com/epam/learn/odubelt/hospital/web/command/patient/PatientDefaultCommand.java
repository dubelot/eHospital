package com.epam.learn.odubelt.hospital.web.command.patient;

import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.entity.Patient;
import com.epam.learn.odubelt.hospital.web.command.Command;
import com.epam.learn.odubelt.hospital.web.command.CommonCommands;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Patient default command. Opens default page for patient with list of appointed doctors
 *
 * @author O.Dubelt
 *
 */
public class PatientDefaultCommand extends Command {
    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger log = Logger.getLogger(PatientDefaultCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.debug("Command starts");

        Patient patient = (Patient) request.getSession().getAttribute(Parameters.USER);
        Long id = patient.getId();
        CommonCommands.setPersonalAndCategoriesForPatient(log, request, id);

        log.debug("Command finished");

        return Path.PAGE_PATIENT_DEFAULT;
    }
}
