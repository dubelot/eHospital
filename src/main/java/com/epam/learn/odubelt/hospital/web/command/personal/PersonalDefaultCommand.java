package com.epam.learn.odubelt.hospital.web.command.personal;

import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.PatientDao;
import com.epam.learn.odubelt.hospital.db.PersonalDao;
import com.epam.learn.odubelt.hospital.db.entity.Patient;
import com.epam.learn.odubelt.hospital.db.entity.Personal;
import com.epam.learn.odubelt.hospital.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
/**
 * Personal default command. Opens default page for personal of hospital with list of appointed patients;
 *
 * @author O.Dubelt
 *
 */
public class PersonalDefaultCommand extends Command {
    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger log = Logger.getLogger(PersonalDefaultCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.debug("Command starts");
        Personal personal = (Personal) request.getSession().getAttribute(Parameters.USER);
        Long id = personal.getId();
        Personal doctor;
        if (personal.getRoleId()==2) {
            doctor = new PersonalDao().findDoctorForNurse(id);
        } else {
            doctor = personal;
        }
        List<Patient> patientList = new PatientDao().getPatientListForDoctor(doctor.getId());
        log.trace("Found in DB: patientList --> " + patientList);
        request.setAttribute(Parameters.PATIENT_LIST,patientList);
        log.trace("Set the request attribute: patientList --> " + patientList);

        log.debug("Command finished");

        return Path.PAGE_PERSONAL_DEFAULT;
    }
}
