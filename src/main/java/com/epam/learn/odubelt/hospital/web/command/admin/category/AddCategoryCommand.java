package com.epam.learn.odubelt.hospital.web.command.admin.category;

import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.CategoryDao;
import com.epam.learn.odubelt.hospital.db.entity.*;
import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.command.Command;
import com.epam.learn.odubelt.hospital.web.command.CommonCommands;
import com.epam.learn.odubelt.hospital.web.command.ValidationException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Command for adding new category.
 *
 * @author O.Dubelt
 *
 */
public class AddCategoryCommand extends Command {
    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger log = Logger.getLogger(AddCategoryCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.debug("Command starts");

        Category category = new Category();
        try {
            new CategoryUpdater().updateCategoryFromRequest(request, category);
        } catch (ValidationException e) {
            CommonCommands.setAttributesFromAllParameters(request);
            request.setAttribute(Parameters.ERROR_MESSAGE, e.getMessage());
            log.trace("Validation error --> " + e.getMessage());
            return Path.PAGE_FORM_CATEGORY;
        }
        //insert category to db
        new CategoryDao().insertCategory(category);
        log.trace("Insert category to db --> " + category);

        log.debug("Command finished");
        return Path.COMMAND_SUCCESS;
    }
}
