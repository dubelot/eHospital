package com.epam.learn.odubelt.hospital.web.command;

import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.web.Parameters;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Logout command.
 * 
 * @author O.Dubelt
 * 
 */
public class LogoutCommand extends Command {

	private static final long serialVersionUID = -2785976616686657267L;

	private static final Logger log = Logger.getLogger(LogoutCommand.class);

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		log.debug("Command starts");
		
		HttpSession session = request.getSession(false);
		if (session != null)
			session.invalidate();
		String localeToSet =  request.getParameter(Parameters.LOCALE_TO_SET);
		log.trace("Request parameter: localeToSet --> " + localeToSet);
		if(!localeToSet.isEmpty()) request.setAttribute(Parameters.DEFAULT_LOCALE, localeToSet);
		log.trace("Request set attribute: defaultLocale --> " + localeToSet);

		log.debug("Command finished");
		return Path.PAGE_LOGIN;
	}

}