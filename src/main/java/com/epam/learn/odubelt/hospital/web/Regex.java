package com.epam.learn.odubelt.hospital.web;
/**
 * Holder for regex.
 *
 * @author O.Dubelt
 *
 */
public final class Regex {

    public static final String UA_NAMES = "^[\u0410-\u042f\u0404\u0406\u0407][\u0430-\u044f\u0454\u0456\u0457\u0027]{0,60}$";
    public static final String EN_NAMES = "^[A-Z][a-z]{0,60}$";
    public static final String RU_NAMES = "^[\u0410-\u042f\u0401][\u0430-\u044f\u0451]{0,60}$";
    public static final String LOGIN = "^[a-z0-9_-]{3,16}$";
    public static final String PASSWORD = "^[A-Za-z0-9\u0410-\u042f\u0401\u0404\u0406\u0407\u0430-\u044f\u0451\u0454\u0456\u0457_-]$";
    public static final String ID_NUMBER = "^[0-9]$";
    public static final String PASSPORT = "^[A-Z\u0410-\u042f\u0401\u0404\u0406\u0407]{0,2}[0-9]{6}$";
}
