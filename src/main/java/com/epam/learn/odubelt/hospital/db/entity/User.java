package com.epam.learn.odubelt.hospital.db.entity;

/**
 * Parent of all user entities.
 * 
 * @author O.Dubelt
 * 
 */
@javax.persistence.Entity
public abstract class User extends Entity {

	private static final long serialVersionUID = -261804448153899083L;

	private String login;

	private String password;

	private String firstName;

	private String lastName;

	private String idNumber;

	private String locale;

	private int active;

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	@Override
	public String toString() {
		return "User [id=" + getId()+ ", login=" + getLogin() + ", password=" + getPassword()
				+ ", firstName=" + getFirstName() + ", lastName=" + getLastName()
				+ ", locale=" + getLocale() + ']';
	}


}
