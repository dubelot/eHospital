package com.epam.learn.odubelt.hospital.web.tag;

import com.epam.learn.odubelt.hospital.db.PersonalDao;
import com.epam.learn.odubelt.hospital.db.entity.Personal;
import com.epam.learn.odubelt.hospital.web.Parameters;
import org.apache.log4j.Logger;
import javax.servlet.jsp.tagext.TagSupport;
/**
 * JSP Tag for getting personal object by id
 *
 * @author O.Dubelt
 */
public class PersonalTag extends TagSupport {
    private static final long serialVersionUID = 6544856157948879418L;

    private String personalId;
    private static final Logger log = Logger.getLogger(PersonalTag.class);
    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    @Override
    public int doStartTag() {

        if (personalId ==null || personalId.isEmpty()) return 0;
        Long id = Long.parseLong(personalId);
        Personal personal = new PersonalDao().findPersonal(id);
        pageContext.setAttribute(Parameters.PERSONAL, personal);

        return EVAL_BODY_INCLUDE;
    }
}
