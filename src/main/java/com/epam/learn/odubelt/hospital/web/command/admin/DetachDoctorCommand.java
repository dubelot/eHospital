package com.epam.learn.odubelt.hospital.web.command.admin;

import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.PatientHasDoctorDao;
import com.epam.learn.odubelt.hospital.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Command for detaching doctor from a patient.
 *
 * @author O.Dubelt
 *
 */
public class DetachDoctorCommand extends Command {
    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger log = Logger.getLogger(DetachDoctorCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.debug("Command starts");

        long patientId = Long.parseLong(request.getParameter(Parameters.PATIENT_ID));
        long personalId = Long.parseLong(request.getParameter(Parameters.PERSONAL_ID));

        if(patientId!=0 && personalId!=0){
            new PatientHasDoctorDao().deleteEntry(patientId, personalId);
        }
        log.debug("Command finished");
        return Path.COMMAND_SUCCESS;
    }
}
