package com.epam.learn.odubelt.hospital.web.command.personal;

import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.PatientDao;
import com.epam.learn.odubelt.hospital.db.entity.Patient;
import com.epam.learn.odubelt.hospital.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/**
 * Command for opening form for adding medical card entry
 *
 * @author O.Dubelt
 *
 */
public class OpenEntryFormCommand extends Command {
    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger log = Logger.getLogger(OpenEntryFormCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.debug("Command starts");
        String entryType = request.getParameter(Parameters.TYPE);
        Long id = Long.parseLong(Optional.ofNullable(request.getParameter(Parameters.ID)).orElse("0"));
        String path;
        if (entryType==null || entryType.isEmpty()){
            request.setAttribute(Parameters.ID, id);
            log.trace("Set the request attribute: id --> " + id);
            path = Path.PAGE_ENTRY_TYPE_SELECT;
        } else {
            request.setAttribute(Parameters.TYPE, entryType);
            log.trace("Set the request attribute: type --> " + entryType);
            Patient patient = new PatientDao().findPatient(id);
            request.setAttribute(Parameters.PATIENT,patient);
            log.trace("Set the request attribute: patient --> " + patient);
            path = Path.PAGE_ENTRY_FORM;
        }
        log.debug("Command finished");
        return path;
    }
}
