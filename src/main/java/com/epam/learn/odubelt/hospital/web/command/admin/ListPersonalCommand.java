package com.epam.learn.odubelt.hospital.web.command.admin;

import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.CategoryDao;
import com.epam.learn.odubelt.hospital.db.PersonalDao;
import com.epam.learn.odubelt.hospital.db.entity.Category;
import com.epam.learn.odubelt.hospital.db.entity.Entity;
import com.epam.learn.odubelt.hospital.db.entity.Personal;
import com.epam.learn.odubelt.hospital.web.command.Command;
import com.epam.learn.odubelt.hospital.web.command.CommonCommands;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;

/**
 * Personal list.
 * 
 * @author O.Dubelt
 * 
 */
public class ListPersonalCommand extends Command {

	private static final long serialVersionUID = 1863978254689586513L;
	private static final Logger log = Logger.getLogger(ListPersonalCommand.class);
	private static final int PAGE_CAPACITY = 10;
	private static final Comparator<Entity> compareById = Comparator.comparing(Entity::getId);

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		log.debug("Commands starts");

		List<Category> categoryList = new CategoryDao().getCategoryList();
		categoryList.sort(compareById);
		request.setAttribute("categoryList", categoryList);
		log.trace("Set the request attribute: categoryList --> " + categoryList);

		String orderBy = request.getParameter(Parameters.ORDER_BY);
		if ((orderBy==null) || orderBy.isEmpty()) orderBy= Parameters.NAME;
		request.setAttribute(Parameters.ORDER_BY, orderBy);
		log.trace("Set the request attribute: orderBy --> " + orderBy);

		int size = new PersonalDao().listSize();
		int currentPage = CommonCommands.setPaginationParameters(request, size, PAGE_CAPACITY);
		log.trace("Set the request attribute: currentPage --> " + currentPage);

		boolean desc = Boolean.parseBoolean(request.getParameter(Parameters.DESC));
		request.setAttribute(Parameters.DESC, desc);
		log.trace("Set the request attribute: desc --> " + desc);

		String locale = (String)request.getSession().getAttribute(Parameters.DEFAULT_LOCALE);
		List<Personal> personalList = new PersonalDao().getPersonalList(currentPage, orderBy, locale, desc);
		request.setAttribute(Parameters.PERSONAL_LIST, personalList);
		log.trace("Set the request attribute: personalList --> " + personalList);

		log.debug("Commands finished");
		return Path.PAGE_LIST_PERSONAL;
	}

}