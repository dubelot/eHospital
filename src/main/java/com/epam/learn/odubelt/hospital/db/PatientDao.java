package com.epam.learn.odubelt.hospital.db;

import com.epam.learn.odubelt.hospital.db.entity.Patient;
import com.epam.learn.odubelt.hospital.web.Parameters;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Data access object for Patient entity.
 */
public class PatientDao {
    private static final String SQL_LIST_PATIENT_SIZE =
            "SELECT COUNT(id) FROM personal";
    private static final String SQL_FIND_PATIENT_LIST =
            "SELECT * FROM patient ORDER BY last_name, first_name LIMIT ?, ?";
    private static final String SQL_FIND_PATIENT_LIST_FOR_DOCTOR =
            "SELECT p.* FROM patient p RIGHT JOIN patient_has_doctor pd on p.id = pd.patient_id WHERE pd.doctor_id=? ORDER BY last_name, first_name";
    private static final String SQL_FIND_PATIENT_BY_LOGIN =
            "SELECT * FROM patient WHERE login=?";
    private static final String SQL_FIND_PATIENT_BY_ID =
            "SELECT * FROM patient WHERE id=?";
    private static final String SQL_FIND_PATIENT_BY_MED_CARD_ID = "SELECT * FROM patient WHERE med_card_id=?";
    private static final String SQL_INSERT_PATIENT =
            "INSERT INTO patient (login, password, first_name, last_name, id_number, birthday, " +
                    "med_card_id) VALUES(?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE_PATIENT =
            "UPDATE patient SET password=?, first_name=?, last_name=?, id_number=?, locale=?, birthday=?"+
                    ", med_card_id=? WHERE id=?";
    private static final String SQL_UPDATE_PATIENT_APPLICATION =
            "UPDATE patient SET application=? WHERE id=?";
    private static final String SQL_DELETE_PATIENT = "DELETE FROM patient WHERE id=?";

    /**
     * Counts all patients in database
     * @return size of patient table
     */
    public int listSize() {
        Statement stmt;
        ResultSet rs;
        Connection con = null;
        int size = 0;
        try {
            con = DBManager.getInstance().getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL_LIST_PATIENT_SIZE);
            rs.next();
            size = rs.getInt(1);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
                DBManager.getInstance().commitAndClose(con);
        }
        return size;
    }

    /**
     * Gets patient list for current page
     * @param currentPage number of page
     * @param orderBy field for sorting
     * @param desc order of list
     * @return list of patients
     */
    public List<Patient> getPatientList(int currentPage, String orderBy, boolean desc) {
        ArrayList<Patient> list = new ArrayList<>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            String sql = SQL_FIND_PATIENT_LIST;
            StringBuffer sbSql;
            StringBuffer sbOrder;
            if (Parameters.NAME.equals(orderBy) && desc) {
                sbSql = new StringBuffer(sql);
                sbOrder = new StringBuffer();
                sbOrder.append(" DESC");
                int offset = sbSql.lastIndexOf(Fields.USER_LAST_NAME) + Fields.USER_LAST_NAME.length();
                sbSql.insert(offset, sbOrder);
                offset = sbSql.lastIndexOf(Fields.USER_FIRST_NAME) + Fields.USER_FIRST_NAME.length();
                sbSql.insert(offset, sbOrder);
                sql = sbSql.toString();
            }
            if (Parameters.BIRTHDAY.equals(orderBy)){
                sbSql = new StringBuffer(sql);
                sbOrder = new StringBuffer();
                sbOrder.append(Fields.PATIENT_BIRTHDAY);
                if (desc) sbOrder.append(" DESC");
                sbOrder.append(", ");
                int offset = sbSql.indexOf("ORDER BY ")+"ORDER BY ".length();
                sbSql.insert(offset, sbOrder);
                sql = sbSql.toString();
            }
            if (Fields.PATIENT_APPLICATION.equals(orderBy)){
                sbSql = new StringBuffer(sql);
                sbOrder = new StringBuffer();
                sbOrder.append(Fields.PATIENT_APPLICATION);
                if (desc) sbOrder.append(" DESC");
                sbOrder.append(", ");
                int offset = sbSql.indexOf("ORDER BY ")+"ORDER BY ".length();
                sbSql.insert(offset, sbOrder);
                sql = sbSql.toString();
            }
            con = DBManager.getInstance().getConnection();
            PatientMapper mapper = new PatientMapper();
            pstmt = con.prepareStatement(sql);
            int k = 1;
            pstmt.setInt(k++, (currentPage - 1) * 10);
            pstmt.setInt(k, currentPage * 10);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(mapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
        return list;
    }
    /**
     * Returns a patient with the given identifier.
     *
     * @param id
     *            Patient identifier.
     * @return Patient entity.
     */
    public Patient findPatient(Long id) {
        Patient patient = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PatientMapper mapper = new PatientMapper();
            pstmt = con.prepareStatement(SQL_FIND_PATIENT_BY_ID);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next())
                patient = mapper.mapRow(rs);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
        return patient;
    }

    /**
     * Returns a patient with the given login.
     *
     * @param login
     *            Patient login.
     * @return Patient entity.
     */
    public Patient findPatientByLogin(String login) {
        Patient patient = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PatientMapper mapper = new PatientMapper();
            pstmt = con.prepareStatement(SQL_FIND_PATIENT_BY_LOGIN);
            pstmt.setString(1, login);
            rs = pstmt.executeQuery();
            if (rs.next())
                patient = mapper.mapRow(rs);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
        return patient;
    }
    /**
     * Returns a patient with the given medical card identifier.
     *
     * @param medCardId
     *            Medical card identifier.
     * @return Patient entity.
     */
    public Patient findPatientByMedCardId(Long medCardId) {
        Patient patient = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PatientMapper mapper = new PatientMapper();
            pstmt = con.prepareStatement(SQL_FIND_PATIENT_BY_MED_CARD_ID);
            pstmt.setLong(1, medCardId);
            rs = pstmt.executeQuery();
            if (rs.next())
                patient = mapper.mapRow(rs);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
                DBManager.getInstance().commitAndClose(con);
        }
        return patient;
    }

    /**
     * Gets all patient appointed to given doctor
     * @param doctorId doctor identifier at personal table
     * @return list of patient appointed to given doctor
     */
    public List<Patient> getPatientListForDoctor(Long doctorId) {
        ArrayList<Patient> list = new ArrayList<>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PatientMapper mapper = new PatientMapper();
            pstmt = con.prepareStatement(SQL_FIND_PATIENT_LIST_FOR_DOCTOR);
            pstmt.setLong(1, doctorId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(mapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
        return list;
    }
    /**
     * Insert patient.
     *
     * @param patient
     *            patient to insert.
     */
    public void insertPatient (Patient patient) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            Long medCardId = new MedCardEntriesDAO().createMedCard();
            patient.setMedCardId(medCardId);
            insertPatient(con, patient);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            throw new IllegalStateException(ex);
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * Insert patient.
     *
     * @param patient
     *            patient to insert.
     * @throws SQLException if insert not valid data
     */
    public void insertPatient(Connection con, Patient patient) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL_INSERT_PATIENT);
        int k = 1;
        pstmt.setString(k++, patient.getLogin());
        pstmt.setString(k++, patient.getPassword());
        pstmt.setString(k++, patient.getFirstName());
        pstmt.setString(k++, patient.getLastName());
        pstmt.setString(k++, patient.getIdNumber());
        pstmt.setDate(k++, patient.getBirthday());
        pstmt.setLong(k, patient.getMedCardId());
        pstmt.executeUpdate();
        pstmt.close();
    }

    /**
     * Update patient.
     *
     * @param patient
     *            patient to update.
     */
    public void updatePatient (Patient patient) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            updatePatient(con, patient);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
    }
    /**
     * Update patient.
     *
     * @param patient
     *            patient to update.
     * @throws SQLException if insert not valid data
     */
    public void updatePatient(Connection con, Patient patient) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL_UPDATE_PATIENT);
        int k = 1;
        pstmt.setString(k++, patient.getPassword());
        pstmt.setString(k++, patient.getFirstName());
        pstmt.setString(k++, patient.getLastName());
        pstmt.setString(k++, patient.getIdNumber());
        pstmt.setString(k++, patient.getLocale());
        pstmt.setDate(k++, patient.getBirthday());
        pstmt.setLong(k++, patient.getMedCardId());
        pstmt.setLong(k, patient.getId());
        pstmt.executeUpdate();
        pstmt.close();
    }

    /**
     * Updates value of patient application
     * @param patientId patient identifier
     * @param application text for insert
     */
    public void updatePatientApplication (Long patientId, String application) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PreparedStatement pstmt = con.prepareStatement(SQL_UPDATE_PATIENT_APPLICATION);
            int k = 1;
            pstmt.setString(k++, application);
            pstmt.setLong(k, patientId);
            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            throw new RuntimeException(ex);
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
    }
    /**
     * Deletes patient.
     *
     * @param id  patient id to delete.
     * @exception SQLRestrictException if database restrict deleting a patient related to doctors or medical card with entries
     */
    public void deletePatient(Long id) throws SQLRestrictException {
        PreparedStatement pstmt;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_DELETE_PATIENT);
            pstmt.setLong(1, id);
            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            throw new SQLRestrictException("It is not possible to delete patient related to doctors or medical card with entries", ex);
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * Extracts a patient from the result set row.
     */
    private static class PatientMapper implements EntityMapper<Patient> {

        @Override
        public Patient mapRow(ResultSet rs) {
            try {
                Patient patient = new Patient();
                patient.setId(rs.getLong(Fields.ENTITY_ID));
                patient.setLogin(rs.getString(Fields.USER_LOGIN));
                patient.setPassword(rs.getString(Fields.USER_PASSWORD));
                patient.setFirstName(rs.getString(Fields.USER_FIRST_NAME));
                patient.setLastName(rs.getString(Fields.USER_LAST_NAME));
                patient.setIdNumber(rs.getString(Fields.USER_ID_NUMBER));
                patient.setLocale(rs.getString(Fields.USER_LOCALE));
                patient.setBirthday(rs.getDate(Fields.PATIENT_BIRTHDAY));
                patient.setMedCardId(rs.getLong(Fields.PATIENT_MED_CARD_ID));
                patient.setApplication(rs.getString(Fields.PATIENT_APPLICATION));
                return patient;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
