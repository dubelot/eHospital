package com.epam.learn.odubelt.hospital.db;

import com.epam.learn.odubelt.hospital.db.entity.Prescription;

import java.sql.*;

/**
 * Data access object for prescription entity.
 */
public class PrescriptionDao {

    private static final String SQL_INSERT_PRESCRIPTION =
            "INSERT INTO prescription (medicine, `procedure`, surgery) VALUES (?,?,?)";
    private static final String SQL_SELECT_LAST_PRESCRIPTION = "SELECT * FROM prescription ORDER BY id DESC LIMIT 1";
    private static final String SQL_SELECT_PRESCRIPTION = "SELECT * FROM prescription WHERE id=?";
        /**
         * Create prescription.
         * @param prescription to create
         * @return id of created Prescription
         *
         */
    public Long createPrescription(Prescription prescription) {
        Long id = null;
        Connection con = null;
        PreparedStatement pstmt;
        ResultSet rs;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_INSERT_PRESCRIPTION);
            int k = 1;
            pstmt.setString(k++, prescription.getMedicine());
            pstmt.setString(k++, prescription.getProcedure());
            pstmt.setString(k, prescription.getSurgery());
            pstmt.executeUpdate();
            rs = pstmt.executeQuery(SQL_SELECT_LAST_PRESCRIPTION);
            if (rs.next()) {
                id = rs.getLong(Fields.ENTITY_ID);
            }
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            throw new RuntimeException(ex);
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
        return id;
    }

    /**
     * Finds prescription for given identifier
     * @param id prescription identifier
     * @return Prescription entity
     */
    public Prescription findPrescription(Long id) {
        Connection con = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Prescription prescription = null;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_SELECT_PRESCRIPTION);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            PrescriptionMapper mapper = new PrescriptionMapper();
            if (rs.next()) prescription = mapper.mapRow(rs);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            throw new RuntimeException(ex);
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
        return prescription;
    }
    /**
     * Extracts a prescription from the result set row.
     */
    private static class PrescriptionMapper implements EntityMapper<Prescription> {

        @Override
        public Prescription mapRow(ResultSet rs) {
            try {
                Prescription prescription = new Prescription();
                prescription.setId(rs.getLong(Fields.ENTITY_ID));
                prescription.setMedicine(rs.getString(Fields.PRESCRIPTION_MEDICINE));
                prescription.setProcedure(rs.getString(Fields.PRESCRIPTION_PROCEDURE));
                prescription.setSurgery(rs.getString(Fields.PRESCRIPTION_SURGERY));
                return prescription;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
