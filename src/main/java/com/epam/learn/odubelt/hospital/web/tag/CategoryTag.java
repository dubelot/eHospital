package com.epam.learn.odubelt.hospital.web.tag;


import com.epam.learn.odubelt.hospital.db.CategoryDao;
import com.epam.learn.odubelt.hospital.db.entity.Category;
import com.epam.learn.odubelt.hospital.web.Parameters;
import org.apache.log4j.Logger;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * JSP Tag for getting category name by current locale
 *
 * @author O.Dubelt
 */
public class CategoryTag extends TagSupport {
    private static final long serialVersionUID = 6544856157948879418L;

    private String categoryId;
    private static final Logger log = Logger.getLogger(CategoryTag.class);

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public int doStartTag(){

        Long id = Long.parseLong(categoryId);
        Category category = new CategoryDao().findCategory(id);
        if (category==null) return 0;

        Object obj = pageContext.getSession().getAttribute(Parameters.DEFAULT_LOCALE);
        if (!(obj instanceof String)) return 0;
        String locale = (String) obj;
        String value;

        switch (locale){
            case Parameters.UA: value = category.getNameUa();
                break;
            case Parameters.RU: value = category.getNameRu();
                break;
            default: value = category.getNameEn();
        }
        pageContext.setAttribute(Parameters.VALUE, value);

        return EVAL_BODY_INCLUDE;
    }

}
