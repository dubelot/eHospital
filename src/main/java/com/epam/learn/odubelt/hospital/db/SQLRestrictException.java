package com.epam.learn.odubelt.hospital.db;

import java.sql.SQLException;

/**
 * Class for handling exception thrown when database restricts on delete or update field values.
 */
public class SQLRestrictException extends SQLException {

    private static final long serialVersionUID = -373028167873998215L;

    public SQLRestrictException(String reason, Throwable cause) {
        super(reason, cause);
    }
}
