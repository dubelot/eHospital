package com.epam.learn.odubelt.hospital.db;

import com.epam.learn.odubelt.hospital.db.entity.Personal;
import com.epam.learn.odubelt.hospital.web.Parameters;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Data access object for Personal entity.
 */
public class PersonalDao {

    private static final String SQL_FIND_ALL_PERSONAL =
            "SELECT p.*, count(pd.doctor_id) AS count FROM personal p LEFT JOIN patient_has_doctor pd on p.id = pd.doctor_id " +
                    "GROUP BY p.id ORDER BY p.id";
    private static final String SQL_FIND_ALL_NURSE =
            "SELECT p.*, 0 AS count  FROM personal p WHERE p.personal_role_id=2 ORDER BY p.last_name, p.first_name";
    private static final String SQL_FIND_PERSONAL_LIST =
            "SELECT p.*, c.*, count(pd.doctor_id) AS count FROM personal p " +
                    "INNER JOIN category c on p.category_id = c.id LEFT JOIN patient_has_doctor pd on p.id = pd.doctor_id " +
                    "GROUP BY p.id, p.last_name, p.first_name ORDER BY p.last_name, p.first_name LIMIT ?, ?";
    private static final String SQL_LIST_PERSONAL_SIZE =
            "SELECT COUNT(id) FROM personal";
    private static final String SQL_FIND_PERSONAL_BY_LOGIN =
            "SELECT *, 0 AS count FROM personal WHERE login=?";
    private static final String SQL_FIND_PERSONAL_BY_ID =
            "SELECT *, 0 AS count FROM personal WHERE id=?";
    private static final String SQL_FIND_PERSONAL_BY_NURSE_ID =
            "SELECT *, 0 AS count FROM personal WHERE nurse_id=?";
    private static final String SQL_INSERT_PERSONAL =
            "INSERT INTO personal (login, password, first_name, last_name, id_number, category_id, " +
                    "personal_role_id, nurse_id) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE_PERSONAL =
            "UPDATE personal SET password=?, first_name=?, last_name=?, id_number=?, locale=?, active=?, category_id=?"+
                    ", personal_role_id=?, nurse_id=? WHERE id=?";
    private static final String SQL_DELETE_PERSONAL =
            "DELETE FROM personal WHERE id=?";
    private static final String SQL_FIND_PERSONAL_FOR_PATIENT =  "SELECT p.*, 0 AS count FROM personal p " +
            "LEFT JOIN patient_has_doctor pd on p.id = pd.doctor_id " +
            "WHERE pd.patient_id=? ORDER BY p.last_name, p.first_name";
    private static final String SQL_FIND_DOCTOR_BY_CATEGORY = "SELECT p.*, count(pd.doctor_id) AS count FROM personal p " +
            "LEFT JOIN patient_has_doctor pd on p.id = pd.doctor_id WHERE p.personal_role_id=1 AND p.category_id=? " +
            "GROUP BY p.id, p.last_name, p.first_name ORDER BY p.last_name, p.first_name";
    private static final String SQL_UPDATE_DETACH_NURSE = "UPDATE personal SET nurse_id=null WHERE id=?";

    /**
     * Counts all personal in database
     * @return size of personal table
     */
    public int listSize() {
        Statement stmt;
        ResultSet rs;
        Connection con = null;
        int size = 0;
        try {
            con = DBManager.getInstance().getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL_LIST_PERSONAL_SIZE);
            rs.next();
            size = rs.getInt(1);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
                DBManager.getInstance().commitAndClose(con);
        }
        return size;
    }

    /**
     * Gets list of all personal in database
     * @return list of personal
     */
    public List<Personal> getPersonalList(){
        ArrayList<Personal> list = new ArrayList<>();
        Statement stmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PersonalMapper mapper = new PersonalMapper();
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL_FIND_ALL_PERSONAL);
            while (rs.next()) {
                list.add(mapper.mapRow(rs));
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
        return list;
    }

    /**
     * Gets list of personal for current page in view
     * @param currentPage number of page
     * @param orderBy field for sorting
     * @param locale  current language
     * @param desc order of list
     * @return list of personal
     */
    public List<Personal> getPersonalList(int currentPage, String orderBy, String locale, boolean desc) {
        ArrayList<Personal> list = new ArrayList<>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            String sql = SQL_FIND_PERSONAL_LIST;
            StringBuffer sbSql;
            StringBuffer sbOrder;
            if (Parameters.NAME.equals(orderBy)){
                sbSql = new StringBuffer(sql);
                sbOrder = new StringBuffer();
                if (desc) {
                    sbOrder.append(" DESC");
                    int offset = sbSql.lastIndexOf("p.last_name") + "p.last_name".length();
                    sbSql.insert(offset, sbOrder);
                    offset = sbSql.lastIndexOf("p.first_name") + "p.first_name".length();
                    sbSql.insert(offset, sbOrder);
                    sql = sbSql.toString();
                }
            }
            if (Parameters.CATEGORY.equals(orderBy)){
                sbSql = new StringBuffer(sql);
                sbOrder = new StringBuffer();
                sbOrder.append("name_").append(locale);
                if (desc) sbOrder.append(" DESC");
                sbOrder.append(", ");
                int offset = sbSql.indexOf("ORDER BY ")+"ORDER BY ".length();
                sbSql.insert(offset, sbOrder);
                sql = sbSql.toString();
            }
            if ("count".equals(orderBy)){
                sbSql = new StringBuffer(sql);
                sbOrder = new StringBuffer();
                sbOrder.append("count");
                if (!desc) {
                    sbOrder.append(" DESC");
                }
                sbOrder.append(", ");
                int offset = sbSql.indexOf("ORDER BY ")+"ORDER BY ".length();
                sbSql.insert(offset, sbOrder);
                sql = sbSql.toString();
            }
            con = DBManager.getInstance().getConnection();
            PersonalMapper mapper = new PersonalMapper();
            pstmt = con.prepareStatement(sql);
            int k = 1;
            pstmt.setInt(k++, (currentPage - 1) * 10);
            pstmt.setInt(k, currentPage * 10);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(mapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
        return list;
    }

    /**
     * Gets all doctors appointed to given patient
     * @param patientId patient identifier
     * @return list of doctors
     */
    public List<Personal> getPersonalListForPatient(Long patientId) {
        ArrayList<Personal> list = new ArrayList<>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PersonalMapper mapper = new PersonalMapper();
            pstmt = con.prepareStatement(SQL_FIND_PERSONAL_FOR_PATIENT);
            pstmt.setLong(1, patientId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(mapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
                DBManager.getInstance().commitAndClose(con);
        }
        return list;
    }

    /**
     * Gets all doctors from personal table with given category
     * @param categoryId category identifier
     * @return list of doctors
     */
    public List<Personal> getDoctorListByCategory(Long categoryId) {
        ArrayList<Personal> list = new ArrayList<>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PersonalMapper mapper = new PersonalMapper();
            pstmt = con.prepareStatement(SQL_FIND_DOCTOR_BY_CATEGORY);
            pstmt.setLong(1, categoryId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(mapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
                DBManager.getInstance().commitAndClose(con);
        }
        return list;
    }

    /**
     * Gets list of all nurse in table personal
     * @return list of nurse
     */
    public List<Personal> getNurseList() {
        ArrayList<Personal> list = new ArrayList<>();
        Statement stmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PersonalMapper mapper = new PersonalMapper();
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL_FIND_ALL_NURSE);
            while (rs.next()) {
                list.add(mapper.mapRow(rs));
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
                DBManager.getInstance().commitAndClose(con);
        }
        return list;
    }
    /**
     * Returns a personal with the given identifier.
     *
     * @param id
     *            Personal identifier.
     * @return Personal entity.
     */
    public Personal findPersonal(Long id) {
        Personal personal = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PersonalMapper mapper = new PersonalMapper();
            pstmt = con.prepareStatement(SQL_FIND_PERSONAL_BY_ID);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next())
                personal = mapper.mapRow(rs);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
        return personal;
    }

    /**
     * Returns a personal with the given login.
     *
     * @param login
     *            Personal login.
     * @return Personal entity.
     */
    public Personal findPersonalByLogin(String login) {
        Personal personal = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PersonalMapper mapper = new PersonalMapper();
            pstmt = con.prepareStatement(SQL_FIND_PERSONAL_BY_LOGIN);
            pstmt.setString(1, login);
            rs = pstmt.executeQuery();
            if (rs.next())
                personal = mapper.mapRow(rs);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
        return personal;
    }

    /**
     * Finds doctor whom given nurse has been appointed
     * @param nurseId nurse identifier
     * @return doctor with given nurse
     */
    public Personal findDoctorForNurse(Long nurseId) {
        Personal personal = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PersonalMapper mapper = new PersonalMapper();
            pstmt = con.prepareStatement(SQL_FIND_PERSONAL_BY_NURSE_ID);
            pstmt.setLong(1, nurseId);
            rs = pstmt.executeQuery();
            if (rs.next())
                personal = mapper.mapRow(rs);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
        return personal;
    }
    /**
     * Update personal.
     *
     * @param personal
     *            personal to update.
     */
    public void updatePersonal (Personal personal) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            updatePersonal(con, personal);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * Update personal.
     *
     * @param personal
     *            personal to update.
     * @throws SQLException if insert not valid data
     */
    public void updatePersonal(Connection con, Personal personal) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL_UPDATE_PERSONAL);
        int k = 1;
        pstmt.setString(k++, personal.getPassword());
        pstmt.setString(k++, personal.getFirstName());
        pstmt.setString(k++, personal.getLastName());
        pstmt.setString(k++, personal.getIdNumber());
        pstmt.setString(k++, personal.getLocale());
        pstmt.setInt(k++, personal.getActive());
        pstmt.setInt(k++, personal.getCategoryId());
        pstmt.setInt(k++, personal.getRoleId());
        Long nurseId = personal.getNurseId();
        if (nurseId==null){
            pstmt.setObject(k++, nurseId, Types.LONGVARBINARY);
        } else {
            pstmt.setLong(k++, nurseId);
        }
        pstmt.setLong(k, personal.getId());
        pstmt.executeUpdate();
        pstmt.close();
    }
    /**
     * Insert personal.
     *
     * @param personal
     *            personal to insert.
     */
    public void insertPersonal (Personal personal) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            insertPersonal(con, personal);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
    }
    /**
     * Insert personal.
     *
     * @param personal
     *            personal to insert.
     * @throws SQLException if insert not valid data
     */
    public void insertPersonal(Connection con, Personal personal) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL_INSERT_PERSONAL);
        int k = 1;
        pstmt.setString(k++, personal.getLogin());
        pstmt.setString(k++, personal.getPassword());
        pstmt.setString(k++, personal.getFirstName());
        pstmt.setString(k++, personal.getLastName());
        pstmt.setString(k++, personal.getIdNumber());
        pstmt.setInt(k++, personal.getCategoryId());
        pstmt.setInt(k++, personal.getRoleId());
        Long nurseId = personal.getNurseId();
        if (nurseId==null){
            pstmt.setObject(k, nurseId, Types.LONGVARBINARY);
        } else {
            pstmt.setLong(k, nurseId);
        }
        pstmt.executeUpdate();
        pstmt.close();
    }

    /**
     * Deletes personal with given identifier
     * @param id personal identifier
     * @throws SQLRestrictException if try to delete hospital staff related to patients or medical card entries
     */
    public void deletePersonal(Long id) throws SQLRestrictException {
        PreparedStatement pstmt;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_DELETE_PERSONAL);
            pstmt.setLong(1, id);
            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            throw new SQLRestrictException("It is not possible to delete hospital staff related to patients or medical card entries", ex);
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
    }
    /**
     * Detaches nurse from given doctor
     * @param doctorId personal identifier
     */
    public void detachNurse(Long doctorId) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PreparedStatement pstmt = con.prepareStatement(SQL_UPDATE_DETACH_NURSE);
            pstmt.setLong(1, doctorId);
            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
    }
    /**
     * Extracts a personal from the result set row.
     */
    private static class PersonalMapper implements EntityMapper<Personal> {

        @Override
        public Personal mapRow(ResultSet rs) {
            try {
                Personal personal = new Personal();
                personal.setId(rs.getLong(Fields.ENTITY_ID));
                personal.setLogin(rs.getString(Fields.USER_LOGIN));
                personal.setPassword(rs.getString(Fields.USER_PASSWORD));
                personal.setFirstName(rs.getString(Fields.USER_FIRST_NAME));
                personal.setLastName(rs.getString(Fields.USER_LAST_NAME));
                personal.setIdNumber(rs.getString(Fields.USER_ID_NUMBER));
                personal.setLocale(rs.getString(Fields.USER_LOCALE));
                personal.setActive(rs.getInt(Fields.USER_ACTIVE));
                personal.setCategoryId(rs.getInt(Fields.PERSONAL_CATEGORY_ID));
                personal.setRoleId(rs.getInt(Fields.PERSONAL_ROLE_ID));
                personal.setNurseId(rs.getObject(Fields.PERSONAL_NURSE_ID, Long.TYPE));
                personal.setCountPatient(rs.getInt(Fields.PERSONAL_COUNT));
                return personal;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
