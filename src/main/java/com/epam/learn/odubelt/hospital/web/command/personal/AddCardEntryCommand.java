package com.epam.learn.odubelt.hospital.web.command.personal;

import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.MedCardEntriesDAO;
import com.epam.learn.odubelt.hospital.db.PrescriptionDao;
import com.epam.learn.odubelt.hospital.db.entity.CardEntry;
import com.epam.learn.odubelt.hospital.db.entity.Personal;
import com.epam.learn.odubelt.hospital.db.entity.Prescription;
import com.epam.learn.odubelt.hospital.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Command for adding medical card entry
 *
 * @author O.Dubelt
 *
 */
public class AddCardEntryCommand extends Command {
    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger log = Logger.getLogger(AddCardEntryCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.debug("Command starts");

        Prescription prescription = createPrescriptionFromRequest(request);
        CardEntry cardEntry = createCardEntryFromRequest(request, prescription);
        log.trace("Set cardEntry --> " + cardEntry);

        log.debug("Command finished");
        return Path.COMMAND_SUCCESS;
    }

    private Prescription createPrescriptionFromRequest(HttpServletRequest request) {
        Prescription prescription = null;
        String medicine = request.getParameter(Parameters.MEDICINE);
        if (medicine!=null && !medicine.isEmpty()) {
            prescription = new Prescription();
            prescription.setMedicine(medicine);
        }
        String procedure = request.getParameter(Parameters.PROCEDURE);
        if (procedure!=null && !procedure.isEmpty()) {
            if (prescription==null) prescription = new Prescription();
            prescription.setProcedure(procedure);
        }
        String surgery = request.getParameter(Parameters.SURGERY);
        if (surgery!=null && !surgery.isEmpty()) {
            if (prescription==null) prescription = new Prescription();
            prescription.setSurgery(surgery);
        }
        if (prescription!=null) {
            Long prescriptionId = new PrescriptionDao().createPrescription(prescription);
            prescription.setId(prescriptionId);
        }
        return prescription;
    }
    private CardEntry createCardEntryFromRequest(HttpServletRequest request, Prescription prescription) {
        CardEntry cardEntry = null;
        if (prescription!=null) {
            cardEntry = new CardEntry();
            cardEntry.setPrescriptionId(prescription.getId());
        }
        String symptoms = request.getParameter(Parameters.SYMPTOMS);
        if (symptoms!=null && !symptoms.isEmpty()) {
            if (cardEntry==null) cardEntry = new CardEntry();
            cardEntry.setSymptoms(symptoms);
        }
        String note = request.getParameter(Parameters.NOTE);
        if (note!=null && !note.isEmpty()) {
            if (cardEntry==null) cardEntry = new CardEntry();
            cardEntry.setNote(note);
        }
        String diagnosis = request.getParameter(Parameters.DIAGNOSIS);
        if (diagnosis!=null && !diagnosis.isEmpty()) {
            if (cardEntry==null) cardEntry = new CardEntry();
            cardEntry.setDiagnosis(diagnosis);
        }
        if (cardEntry!=null) {
            Long personalId = ((Personal) request.getSession().getAttribute(Parameters.USER)).getId();
            cardEntry.setPersonalId(personalId);
            int entryTypeId = Integer.parseInt(request.getParameter(Parameters.ENTRY_TYPE_ID));
            cardEntry.setEntryTypeId(entryTypeId);
            Long medCardId = Long.parseLong(request.getParameter(Parameters.MED_CARD_ID));
            cardEntry.setMedCardId(medCardId);
            new MedCardEntriesDAO().insertCardEntry(cardEntry);
        }
        return cardEntry;
    }
}
