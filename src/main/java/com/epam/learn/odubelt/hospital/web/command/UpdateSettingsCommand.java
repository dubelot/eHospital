package com.epam.learn.odubelt.hospital.web.command;

import com.epam.learn.odubelt.hospital.web.EHospUtility;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.entity.User;
import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.Regex;
import com.epam.learn.odubelt.hospital.web.command.admin.user.UserUpdater;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Update settings items.
 * 
 * @author O.Dubelt
 * 
 */
public class UpdateSettingsCommand extends Command {

	private static final long serialVersionUID = 7732286214029478505L;
	private static final Logger log = Logger.getLogger(UpdateSettingsCommand.class);
	public static final String OLD_PASSWORD = "oldPassword";
	public static final String NEW_PASSWORD = "newPassword";

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		log.debug("Command starts");
		String locale = (String) request.getSession().getAttribute(Parameters.DEFAULT_LOCALE);
		ResourceBundle bundle = ResourceBundle.getBundle(Parameters.RESOURCES, Locale.forLanguageTag(locale));
		User user = (User)request.getSession().getAttribute(Parameters.USER);
		boolean updateUser = false;
		// update password
		String oldPassword = request.getParameter(OLD_PASSWORD);
		String newPassword = request.getParameter(NEW_PASSWORD);
		if (EHospUtility.comparePasswords(oldPassword,user.getPassword())
				&& newPassword!=null
				&& !newPassword.isEmpty()) {
			if (!newPassword.matches(Regex.PASSWORD)) {
				request.setAttribute(Parameters.ERROR_MESSAGE, bundle.getString("regex.user.password"));
				return Path.PAGE_SETTINGS;
			}
			user.setPassword(EHospUtility.hashPassword(newPassword));
			updateUser = true;
		} else {
			request.setAttribute(Parameters.ERROR_MESSAGE, bundle.getString("regex.user.password.wrong"));
			return Path.PAGE_SETTINGS;
		}
		// update locale
		String localeToSet = request.getParameter(Parameters.LOCALE_TO_SET);
		if (localeToSet != null && !localeToSet.isEmpty()) {
			HttpSession session = request.getSession();
			Config.set(session, Parameters.JAVAX_SERVLET_JSP_JSTL_FMT_LOCALE, localeToSet);
			session.setAttribute(Parameters.DEFAULT_LOCALE, localeToSet);
			user.setLocale(localeToSet);
			updateUser = true;
		}
		if (updateUser) new UserUpdater().updateUserInDB(user);
		log.debug("Command finished");
		return Path.PAGE_SETTINGS;
	}
}