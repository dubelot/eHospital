package com.epam.learn.odubelt.hospital.web.command;

import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.*;
import com.epam.learn.odubelt.hospital.db.entity.*;
import com.epam.learn.odubelt.hospital.web.Parameters;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Locale;
import java.util.ResourceBundle;
/**
 * Command for saving medical card entries as PDF file. Returns error page if patient tries to save another patient's medical card entries.
 *
 * @author O.Dubelt
 *
 */
public class SavePDFCommand extends Command {
    private static final long serialVersionUID = 4746672079608598998L;

    public static final String FILE_NAME = "Card_Entry";
    public static final String EXTENSION = ".pdf";
    public static final String EMPTY = "";
    private static int counter=1;
    private static final Logger log = Logger.getLogger(SavePDFCommand.class);
    public static final String FONT = "fonts/arialRegular.ttf";

    @Override
    public String execute(HttpServletRequest request,
                          HttpServletResponse response) throws IOException, ServletException {

        log.debug("Command starts");
        String locale = ((User)request.getSession().getAttribute(Parameters.USER)).getLocale();
        Long id = Long.parseLong(request.getParameter(Parameters.ID));
        CardEntry cardEntry = new MedCardEntriesDAO().findCardEntry(id);
        //restricting access to other patient's medical cards
        User user = (User) request.getSession().getAttribute(Parameters.USER);
        if (user instanceof Patient) {
            if (!((Patient) user).getMedCardId().equals(cardEntry.getMedCardId())) {
                request.setAttribute(Parameters.ERROR_MESSAGE, "Access error");
                return Path.PAGE_ERROR_PAGE;
            }
        }
        Long prescriptionId = cardEntry.getPrescriptionId();
        Prescription prescription = null;
        if (prescriptionId!=null) prescription = new PrescriptionDao().findPrescription(prescriptionId);
        Patient patient = new PatientDao().findPatientByMedCardId(cardEntry.getMedCardId());
        Personal personal = new PersonalDao().findPersonal(cardEntry.getPersonalId());
        Category category = new CategoryDao().findCategory((long)personal.getCategoryId());

        File file = createCardEntryPDF(patient, personal, category, cardEntry, prescription, locale);
        log.trace("Created file --->" + file.getAbsolutePath());

        response.setContentType("application/pdf");
        response.addHeader("Content-Disposition", "attachment; filename=" + file.getPath());
        response.setContentLength((int) file.length());
        FileInputStream fileInputStream = new FileInputStream(file);
        OutputStream responseOutputStream = response.getOutputStream();
        int bytes;
        while ((bytes = fileInputStream.read()) != -1) {
            responseOutputStream.write(bytes);
        }
        log.debug("Command finished");
        return null;
    }

    /**
     * Creates A5 landscape PDF File with card entry
     * @param patient Patient for card entry
     * @param personal Personal for card entry
     * @param category Category of personal
     * @param cardEntry CardEntry object
     * @param prescription Prescription of card entry
     * @param locale language for card entry
     * @return created PDF File;
     */
    private static File createCardEntryPDF(Patient patient, Personal personal, Category category, CardEntry cardEntry, Prescription prescription, String locale) {
        ResourceBundle bundle = ResourceBundle.getBundle(Parameters.RESOURCES, Locale.forLanguageTag(locale));
        Document document = new Document(new RectangleReadOnly(595.0F, 420.0F), 50, 50, 50, 50);
        String path = FILE_NAME +(counter++)+ EXTENSION;
        File file = new File(path);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            PdfWriter.getInstance(document, fos);
            document.open();

            BaseFont bf = BaseFont.createFont(FONT, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font fontH5 = new Font(bf, 14, Font.BOLD);
            Font fontH6 = new Font(bf, 12, Font.BOLD);
            Font font = new Font(bf, 12, Font.ITALIC);
            StringBuilder header = new StringBuilder();
            if (cardEntry.getEntryTypeId()==1) {
                header.append(bundle.getString("jsp_entity.entry_type.appointment"));
            }
            if (cardEntry.getEntryTypeId()==2) {
                header.append(bundle.getString("jsp_entity.entry_type.procedure"));
            }
            if (cardEntry.getEntryTypeId()==3) {
                header.append(bundle.getString("jsp_entity.entry_type.discharge"));
            }
            header.append(' ').append(cardEntry.getCreateTime().toString());
            Chunk chunk = new Chunk(header.toString(), fontH5);
            chunk.setLineHeight(22);
            document.add(chunk);

            PdfPTable table = new PdfPTable(new float[]{200F, 250F, 300F, 300F});
            table.setSpacingBefore(20);

            table.addCell(new Phrase(new Chunk(bundle.getString("jsp_entity_type.patient"), fontH6)));
            StringBuilder cell = new StringBuilder();
            cell.append(patient.getFirstName()).append(' ').append(patient.getLastName());
            table.addCell(new Phrase(new Chunk(cell.toString(), font)));

            table.addCell(new Phrase(new Chunk(bundle.getString("form_user_jsp.date_of_birth"), fontH6)));
            table.addCell(new Phrase(new Chunk(patient.getBirthday().toString(), font)));

            document.add(table);

            table = new PdfPTable(new float[]{200F, 850F});
            table.addCell(new Phrase( new Chunk(bundle.getString("jsp_entity_type.doctor"), fontH6)));
            String cat = category.getNameEn();
            if(Parameters.UA.equals(locale)) cat = category.getNameUa();
            if(Parameters.RU.equals(locale)) cat = category.getNameRu();
            cell = new StringBuilder(cat).append(' ').append(personal.getFirstName()).append(' ').append(personal.getLastName());
            table.addCell(new Phrase(new Chunk(cell.toString(), font)));

            table.addCell(new Phrase(new Chunk(bundle.getString("jsp_entity_type.card_entry.symptoms"), fontH6)));
            String nullable = cardEntry.getSymptoms();
            table.addCell(new Phrase(new Chunk((nullable==null)? EMPTY :nullable, font)));

            table.addCell(new Phrase(new Chunk(bundle.getString("jsp_entity_type.card_entry.note"), fontH6)));
            nullable = cardEntry.getNote();
            table.addCell(new Phrase(new Chunk((nullable==null)? EMPTY :nullable, font)));

            table.addCell(new Phrase(new Chunk(bundle.getString("jsp_entity_type.card_entry.diagnosis"), fontH6)));
            nullable = cardEntry.getDiagnosis();
            table.addCell(new Phrase(new Chunk((nullable==null)? EMPTY :nullable, font)));

            document.add(table);

            table = new PdfPTable(new float[]{1050F});
            table.addCell(new Phrase(new Chunk(bundle.getString("jsp_entity.entry_type.prescription"), fontH6)));

            document.add(table);

            table = new PdfPTable(new float[]{200F, 850F});
            table.addCell(new Phrase(new Chunk(bundle.getString("jsp_entity_type.prescription.medicine"), fontH6)));
            if (prescription!=null) nullable = prescription.getMedicine();
            else {
                nullable=null;
            }
            table.addCell(new Phrase(new Chunk((nullable==null)? EMPTY :nullable, font)));
            table.addCell(new Phrase(new Chunk(bundle.getString("jsp_entity_type.prescription.procedure"), fontH6)));
            if (prescription!=null) nullable = prescription.getProcedure();
            else {
                nullable=null;
            }
            table.addCell(new Phrase(new Chunk((nullable==null)? EMPTY :nullable, font)));
            table.addCell(new Phrase(new Chunk(bundle.getString("jsp_entity_type.prescription.surgery"), fontH6)));
            if (prescription!=null) nullable = prescription.getSurgery();
            else {
                nullable=null;
            }
            table.addCell(new Phrase(new Chunk((nullable==null)? EMPTY :nullable, font)));
            document.add(table);

        } catch (DocumentException | IOException e) {
            log.trace(e.getMessage());
        } finally {
            document.close();
        }
       return file;
    }
}
