package com.epam.learn.odubelt.hospital.web;

/**
 * Path holder (jsp pages, controller commands).
 * 
 * @author O.Dubelt
 * 
 */
public final class Path {
	
	// pages
	public static final String PAGE_LOGIN = "/login.jsp";
	public static final String PAGE_ERROR_PAGE = "/WEB-INF/jsp/error_page.jsp";
	public static final String PAGE_LIST_ADMIN = "/WEB-INF/jsp/admin/list_admin.jsp";
	public static final String PAGE_LIST_PERSONAL = "/WEB-INF/jsp/admin/list_personal.jsp";
	public static final String PAGE_LIST_PATIENT = "/WEB-INF/jsp/admin/list_patient.jsp";
	public static final String PAGE_LIST_CATEGORY = "/WEB-INF/jsp/admin/list_category.jsp";
	public static final String PAGE_SETTINGS = "/WEB-INF/jsp/settings.jsp";

	public static final String PAGE_FORM_USER ="/WEB-INF/jsp/form_user.jsp";
    public static final String PAGE_SUCCESS = "/WEB-INF/jsp/success.jsp";
    public static final String PAGE_FORM_CATEGORY = "/WEB-INF/jsp/admin/form_new_category.jsp";
	public static final String PAGE_FORM_EDIT_CATEGORY = "/WEB-INF/jsp/admin/form_edit_category.jsp";
    public static final String PAGE_FORM_EDIT_USER = "/WEB-INF/jsp/form_edit_user.jsp";

    public static final String PAGE_APPOINT = "/WEB-INF/jsp/admin/form_appoint.jsp";
	public static final String PAGE_PATIENT_DEFAULT ="/WEB-INF/jsp/patient/patient_default.jsp";
	public static final String PAGE_PERSONAL_DEFAULT ="/WEB-INF/jsp/personal/personal_default.jsp";
    public static final String PAGE_ENTRY_FORM = "/WEB-INF/jsp/personal/entry_form.jsp";
    public static final String PAGE_ENTRY_TYPE_SELECT ="/WEB-INF/jsp/personal/entry_type_select.jsp";
    public static final String PAGE_MED_CARD = "/WEB-INF/jsp/med_card.jsp";

	// commands
	public static final String COMMAND_LIST_PERSONAL = "/controller?command=listPersonal";
	public static final String COMMAND_SUCCESS = "/controller?command=success";
	public static final String COMMAND_PATIENT_DEFAULT = "/controller?command=patientDefault";
	public static final String COMMAND_PERSONAL_DEFAULT = "/controller?command=personalDefault";
	public static final String COMMAND_OPEN_EDIT_USER_FORM = "/controller?command=openEditUserForm";
	public static final String COMMAND_OPEN_EDIT_CATEGORY_FORM = "/controller?command=openEditCategoryForm";
}