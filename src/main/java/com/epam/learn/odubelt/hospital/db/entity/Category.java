package com.epam.learn.odubelt.hospital.db.entity;

/**
 * Category entity.
 * 
 * @author O.Dubelt
 * 
 */
@javax.persistence.Entity
public class Category extends Entity {

	private static final long serialVersionUID = -3545525740851787426L;

	private String nameUa;

	public String getNameUa() {
		return nameUa;
	}

	public void setNameUa(String nameUa) {
		this.nameUa = nameUa;
	}

	private String nameEn;

	public String getNameEn() {
		return nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}

	private String nameRu;

	public String getNameRu() {
		return nameRu;
	}

	public void setNameRu(String nameRu) {
		this.nameRu = nameRu;
	}

	@Override
	public String toString() {
		return "Category [id=" + getId() + ", name=" + nameEn + ']';
	}

}
