package com.epam.learn.odubelt.hospital.web.command;

import com.epam.learn.odubelt.hospital.db.CategoryDao;
import com.epam.learn.odubelt.hospital.db.PersonalDao;
import com.epam.learn.odubelt.hospital.db.entity.*;
import com.epam.learn.odubelt.hospital.web.Parameters;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;

/**
 * Common methods for different commands
 */
public final class CommonCommands {
    //start page for pagination
    private static final int START_PAGE = 1;
    /**
     * Sets request attributes for appointed doctors to patient
     * @param log current logger
     * @param request current request
     * @param id Patient id
     */
    public static void setPersonalAndCategoriesForPatient(Logger log, HttpServletRequest request, Long id){
        List<Category> categoryList = new CategoryDao().getCategoryList();
        log.trace("Found in DB: categoryList --> " + categoryList);
        // put category list to request
        request.setAttribute(Parameters.CATEGORY_LIST, categoryList);
        log.trace("Set the request attribute: categoryList --> " + categoryList);

        List<Personal> personalList = new PersonalDao().getPersonalListForPatient(id);
        log.trace("Found in DB: personalList --> " + personalList);
        // put category list to request
        request.setAttribute(Parameters.PERSONAL_LIST,personalList);
        log.trace("Set the request attribute: personalList --> " + personalList);
    }

    /**
     * Set pagination parameters to request attributes and return number of page for drawing.
     * @param request current request
     * @param size size of list for pagination
     * @param capacity number of elements of list on one page
     * @return current page from request for drawing in view
     */
    public static int setPaginationParameters(HttpServletRequest request, int size, int capacity) {
        int countPage = START_PAGE;
        Optional<String> opt = Optional.ofNullable(request.getParameter(Parameters.CURRENT_PAGE));
        int currentPage = Integer.parseInt(opt.orElse(String.valueOf(START_PAGE)));
        request.setAttribute(Parameters.CURRENT_PAGE, currentPage);
        if (size!=0) countPage = (size% capacity ==0)?(size/ capacity):(size/capacity+ START_PAGE);
        request.setAttribute(Parameters.COUNT_PAGE, countPage);
        return currentPage;
    }
    /**
     * Sets all parameters of current request as attributes of new request
     * as values from previous user's submit
     * @param request current request
     */
    public static void setAttributesFromAllParameters(HttpServletRequest request) {
        if (request==null) return;
        Enumeration<String> paramList = request.getParameterNames();
        while (paramList.hasMoreElements()){
            String paramName = paramList.nextElement();
            request.setAttribute(paramName, request.getParameter(paramName));
        }
    }
}
