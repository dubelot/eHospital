package com.epam.learn.odubelt.hospital.web;

import com.epam.learn.odubelt.hospital.web.command.Command;
import com.epam.learn.odubelt.hospital.web.command.CommandContainer;
import org.apache.log4j.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Main servlet controller.
 * 
 * @author O.Dubelt
 * 
 */
public class Controller extends HttpServlet {

	private static final long serialVersionUID = 5188417007500914812L;

	private static final Logger log = Logger.getLogger(Controller.class);
	public static final String SUCCESS = "success";


	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	/**
	 * Main method of this controller.
	 */
	private void process(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		log.debug("Controller starts");

		// extract command name from the request
		String commandName = request.getParameter(Parameters.COMMAND);
		log.trace("Request parameter: command --> " + commandName);

		// obtain command object by its name
		Command command = CommandContainer.get(commandName);
		log.trace("Obtained command --> " + command);
		String localeToSet = request.getParameter(Parameters.LOCALE_TO_SET);
		if (localeToSet != null && !localeToSet.isEmpty()) {
			HttpSession session = request.getSession();
			Config.set(session, Parameters.JAVAX_SERVLET_JSP_JSTL_FMT_LOCALE, localeToSet);
			session.setAttribute(Parameters.DEFAULT_LOCALE, localeToSet);
		}
		// execute command and get forward address
		String forward = command.execute(request, response);
		log.trace("Forward address --> " + forward);

		log.debug("Controller finished, now go to forward address --> " + forward);
		//sendRedirect commands that insert data to database
		if ((forward != null) && forward.endsWith(SUCCESS)){
			response.sendRedirect(request.getContextPath()+forward);	}
		//forward simply commands without inserting data to database
		else if (forward != null ){
			RequestDispatcher disp = request.getRequestDispatcher(forward);
			disp.forward(request, response);
		}
	}
}