package com.epam.learn.odubelt.hospital.web.command.admin.user;

import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.entity.User;
import com.epam.learn.odubelt.hospital.web.command.Command;
import com.epam.learn.odubelt.hospital.web.command.ValidationException;
import org.apache.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Command for editing user.
 *
 * @author O.Dubelt
 *
 */
public class EditUserCommand extends Command {
    private static final long serialVersionUID = -3071536593627692473L;
    public static final String PARAMETER_EDIT_TYPE = "editType";
    private static final Logger log = Logger.getLogger(EditUserCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.debug("Command starts");
        String editType = request.getParameter(PARAMETER_EDIT_TYPE);
        log.trace("Get the request parameter: editType --> " + editType);
        Long id = Long.parseLong(request.getParameter(Parameters.ID));
        log.trace("Get the request parameter: id --> " + id);

        UserUpdater updater = new UserUpdater();
        User user = updater.getUserFromDB(editType, id);
        try {
            updater.updateUserFromRequest(request, user);
        } catch (ValidationException e) {
            request.setAttribute("editType", editType);
            request.setAttribute("id", id);
            request.setAttribute(Parameters.ERROR_MESSAGE, e.getMessage());
            log.trace("Validation error --> " + e.getMessage());
            return Path.COMMAND_OPEN_EDIT_USER_FORM;
        }
        updater.updateUserInDB(user);
        log.trace("Set user --> " + user);

        log.debug("Command finished");
        return Path.COMMAND_SUCCESS;
    }

}
