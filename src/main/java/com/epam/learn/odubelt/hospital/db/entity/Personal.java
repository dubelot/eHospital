package com.epam.learn.odubelt.hospital.db.entity;

import javax.persistence.Entity;

/**
 * Personal entity.
 * 
 * @author O.Dubelt
 * 
 */
@Entity
public class Personal extends User {

	private static final long serialVersionUID = 4273106956940259236L;



	private int categoryId;

	private int roleId;

	private Long nurseId;

	private int countPatient;

	public int getCountPatient() {
		return countPatient;
	}

	public void setCountPatient(int countPatient) {
		this.countPatient = countPatient;
	}

	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public Long getNurseId() {
		return nurseId;
	}

	public void setNurseId(Long nurseId) {
		this.nurseId = nurseId;
	}

	@Override
	public String toString() {
		return "Personal [id=" + getId()+ ", login=" + getLogin() + ", password=" + getPassword()
				+ ", firstName=" + getFirstName() + ", lastName=" + getLastName()
				+ ", locale=" + getLocale() + ", categoryId="+ categoryId + ", roleId="+ roleId +", nurseId="+ nurseId +']';
	}

}
