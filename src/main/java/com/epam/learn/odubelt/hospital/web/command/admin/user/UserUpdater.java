package com.epam.learn.odubelt.hospital.web.command.admin.user;

import com.epam.learn.odubelt.hospital.db.AdminDao;
import com.epam.learn.odubelt.hospital.db.PatientDao;
import com.epam.learn.odubelt.hospital.db.PersonalDao;
import com.epam.learn.odubelt.hospital.db.SQLRestrictException;
import com.epam.learn.odubelt.hospital.db.entity.Admin;
import com.epam.learn.odubelt.hospital.db.entity.Patient;
import com.epam.learn.odubelt.hospital.db.entity.Personal;
import com.epam.learn.odubelt.hospital.db.entity.User;
import com.epam.learn.odubelt.hospital.web.EHospUtility;
import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.Regex;
import com.epam.learn.odubelt.hospital.web.command.ValidationException;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Class for operating user objects
 */
public class UserUpdater {
    /**
     * Updates User object by given request's parameters
     * @param request HttpServletRequest
     * @throws ValidationException if request parameters are not valid
     */
    public void updateUserFromRequest(HttpServletRequest request, User user) throws ValidationException {

        String locale = ((User)request.getSession().getAttribute(Parameters.USER)).getLocale();
        ResourceBundle bundle = ResourceBundle.getBundle(Parameters.RESOURCES, Locale.forLanguageTag(locale));

        String login = request.getParameter(Parameters.LOGIN);
        if (login != null && !login.isEmpty()) {
            if (login.matches(Regex.LOGIN)) {
                user.setLogin(login);
            } else {
                throw new ValidationException(bundle.getString("regex.user.login"));
            }
        }
        String password = request.getParameter(Parameters.PASSWORD);
        if (password != null && !password.isEmpty()){
            if (password.matches(Regex.PASSWORD)) {
                user.setPassword(EHospUtility.hashPassword(password));
            } else {
                throw new ValidationException(bundle.getString("regex.user.password"));
            }
        }
        String firstName = request.getParameter(Parameters.FIRST_NAME);
        if (firstName != null && !firstName.isEmpty()) {
            if (firstName.matches(Regex.UA_NAMES)) {
                user.setFirstName(firstName);
            } else {
                throw new ValidationException(bundle.getString("regex.user.name"));
            }
        }
        String lastName = request.getParameter(Parameters.LAST_NAME);
        if (lastName != null && !lastName.isEmpty()){
            if(lastName.matches(Regex.UA_NAMES)) {
                user.setLastName(lastName);
            } else {
                throw new ValidationException(bundle.getString("regex.user.name"));
            }
        }
        String idNumber = request.getParameter(Parameters.ID_NUMBER);
        if (idNumber != null && !idNumber.isEmpty()){
            if (idNumber.matches(Regex.ID_NUMBER)||idNumber.matches(Regex.PASSPORT)){
                user.setIdNumber(idNumber);
            } else {
                throw new ValidationException(bundle.getString("regex.user.id_number"));
            }
        }
        if (user instanceof Patient) {
            String birthday = request.getParameter(Parameters.BIRTHDAY);
            if (birthday != null && !birthday.isEmpty()) {
                ((Patient)user).setBirthday(Date.valueOf(birthday));
            }
        }
        if (user instanceof Personal) {
            String categoryId = request.getParameter(Parameters.CATEGORY_ID);
            if (categoryId != null && !categoryId.isEmpty()) {
                ((Personal)user).setCategoryId(Integer.parseInt(categoryId));
            }
            String roleId = request.getParameter(Parameters.ROLE_ID);
            if (roleId != null && !roleId.isEmpty()) {
                ((Personal)user).setRoleId(Integer.parseInt(roleId));
            }
            String nurseId = request.getParameter(Parameters.NURSE_ID);
            if (nurseId != null && !nurseId.isEmpty()) {
                ((Personal)user).setNurseId(Long.parseLong(nurseId));
            }
        }
    }
    /**
     * Updates user in database
     *
     * @param user User for updating
     */
    public void updateUserInDB(User user){
        if (user instanceof Admin) {
            new AdminDao().updateAdmin((Admin) user);
        }
        if (user instanceof Patient) {
            new PatientDao().updatePatient((Patient) user);
        }
        if (user instanceof Personal) {
            new PersonalDao().updatePersonal((Personal)user);
        }
    }
    /**
     * Gets user with given type and id from database
     * @param type of given user
     * @param id user identifier
     * @return User object from database
     */
    public User getUserFromDB(String type, Long id) {
        if (Parameters.PATIENT.equals(type)){
            return new PatientDao().findPatient(id);
        }
        if (Parameters.ADMIN.equals(type)){
            return new AdminDao().findAdmin(id);
        }
        return new PersonalDao().findPersonal(id);
    }
    /**
     * Deletes user with given type and id from database
     * @param type of given user
     * @param id user identifier
     */
    public void deleteUserFromDB(String type, Long id) throws SQLRestrictException {
        if (Parameters.PATIENT.equals(type)){
            new PatientDao().deletePatient(id);
        }
        if (Parameters.ADMIN.equals(type)){
            new AdminDao().deleteAdmin(id);
        }
        if (Parameters.PERSONAL.equals(type)) {
            new PersonalDao().deletePersonal(id);
        }
    }
}
