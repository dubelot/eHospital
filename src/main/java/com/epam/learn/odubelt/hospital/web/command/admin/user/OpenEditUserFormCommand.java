package com.epam.learn.odubelt.hospital.web.command.admin.user;

import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.CategoryDao;
import com.epam.learn.odubelt.hospital.db.PersonalDao;
import com.epam.learn.odubelt.hospital.db.entity.*;
import com.epam.learn.odubelt.hospital.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
/**
 * Command for open form for editing user.
 *
 * @author O.Dubelt
 *
 */
public class OpenEditUserFormCommand extends Command {
    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger log = Logger.getLogger(OpenEditUserFormCommand.class);
    private static final Comparator<Category> compareByNameUA = Comparator.comparing(Category::getNameUa);
    private static final Comparator<Category> compareByNameEN = Comparator.comparing(Category::getNameEn);
    private static final Comparator<Category> compareByNameRU = Comparator.comparing(Category::getNameRu);
    public static final String EDIT_TYPE = "editType";
    public static final String USER_EDIT = "userEdit";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.debug("Command starts");
        String locale = ((User)request.getSession().getAttribute(Parameters.USER)).getLocale();

        List<Category> categoryList = new CategoryDao().getCategoryList();
        if (Parameters.UA.equals(locale)) categoryList.sort(compareByNameUA);
        if (Parameters.EN.equals(locale)) categoryList.sort(compareByNameEN);
        if (Parameters.RU.equals(locale)) categoryList.sort(compareByNameRU);
        log.trace("Found in DB: categoryList --> " + categoryList);
        request.setAttribute(Parameters.CATEGORY_LIST, categoryList);
        log.trace("Set the request attribute: categoryList --> " + categoryList);

        String editType = request.getParameter(EDIT_TYPE);
        request.setAttribute(EDIT_TYPE, editType);
        log.trace("Set the request attribute: editType --> " + editType);

        Long id = Long.parseLong(request.getParameter(Parameters.ID));
        User userEdit = new UserUpdater().getUserFromDB(editType, id);
        if (userEdit instanceof Personal) {
            List<Personal> nurseList = new PersonalDao().getNurseList();
            log.trace("Found in DB: nurseList --> " + nurseList);
            request.setAttribute(Parameters.NURSE_LIST, nurseList);
            log.trace("Set the request attribute: nurseList --> " + nurseList);
        }
        request.setAttribute(USER_EDIT, userEdit);
        log.trace("Set the request attribute: userEdit --> " + userEdit);
        log.debug("Command finished");
        return Path.PAGE_FORM_EDIT_USER;
    }
}
