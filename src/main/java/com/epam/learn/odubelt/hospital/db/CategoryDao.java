package com.epam.learn.odubelt.hospital.db;

import com.epam.learn.odubelt.hospital.db.entity.Category;
import com.epam.learn.odubelt.hospital.web.Parameters;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Data access object for Category entity.
 */
public class CategoryDao {

    private static final String SQL_FIND_ALL_CATEGORIES =
            "SELECT * FROM category";
    private static final String SQL_LIST_CATEGORY_SIZE =
            "SELECT COUNT(id) FROM category";
    private static final String SQL_FIND_CATEGORY_LIST_UA =
            "SELECT * FROM category ORDER BY name_ua LIMIT ?, ?";
    private static final String SQL_FIND_CATEGORY_LIST_EN =
            "SELECT * FROM category ORDER BY name_en LIMIT ?, ?";
    private static final String SQL_FIND_CATEGORY_LIST_RU =
            "SELECT * FROM category ORDER BY name_ru LIMIT ?, ?";
    private static final String SQL_FIND_CATEGORY_BY_ID =
            "SELECT * FROM category WHERE id=?";
    private static final String SQL_INSERT_CATEGORY =
            "INSERT INTO category (name_ua, name_en, name_ru) VALUES(?, ?, ?)";
    private static final String SQL_UPDATE_CATEGORY =
            "UPDATE category SET name_ua=?, name_en=?, name_ru=? WHERE id=?";
    public static final String SQL_DELETE_CATEGORY =
            "DELETE FROM category WHERE id=?";
    /**
     * Counts all categories in database
     * @return size of category table
     */
    public int listSize() {
        Statement stmt;
        ResultSet rs;
        Connection con = null;
        int size = 0;
        try {
            con = DBManager.getInstance().getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL_LIST_CATEGORY_SIZE);
            rs.next();
            size = rs.getInt(1);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if (con!=null)
                DBManager.getInstance().commitAndClose(con);
        }
        return size;
    }
    /**
     * Gets list of all categories from database
     * @return list of all categories
     */
    public List<Category> getCategoryList(){
        ArrayList<Category> list = new ArrayList<>();
        Statement stmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            CategoryMapper mapper = new CategoryMapper();
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL_FIND_ALL_CATEGORIES);
            while (rs.next()) {
                list.add(mapper.mapRow(rs));
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if(con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
        return list;
    }
    /**
     * Gets category list for current page.
     * @param currentPage number of page
     * @param locale language for current user
     * @param desc order of list
     * @return list af categories
     */
    public List<Category> getCategoryList(int currentPage, String locale, boolean desc) {
        ArrayList<Category> list = new ArrayList<>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            String sql = SQL_FIND_CATEGORY_LIST_UA;
            if (Parameters.EN.equals(locale)) sql = SQL_FIND_CATEGORY_LIST_EN;
            if (Parameters.RU.equals(locale)) sql = SQL_FIND_CATEGORY_LIST_RU;
            if (desc) {
                StringBuilder sbSql = new StringBuilder(sql);
                int offset = sbSql.lastIndexOf("LIMIT");
                sbSql.insert(offset, "DESC ");
                sql = sbSql.toString();
            }
            con = DBManager.getInstance().getConnection();
            CategoryMapper mapper = new CategoryMapper();
            pstmt = con.prepareStatement(sql);
            int k = 1;
            pstmt.setInt(k++, (currentPage - 1) * 10);
            pstmt.setInt(k, currentPage * 10);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(mapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if(con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
        return list;
    }
     /**
     * Returns a category with the given identifier.
     *
     * @param id
     *            Category identifier.
     * @return Category entity.
     */
    public Category findCategory(Long id) {
        Category category = null;
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            CategoryMapper mapper = new CategoryMapper();
            pstmt = con.prepareStatement(SQL_FIND_CATEGORY_BY_ID);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next())
                category = mapper.mapRow(rs);
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if(con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
        return category;
    }
    /**
     * Update category.
     *
     * @param category
     *            category to update.
     */
    public void updateCategory (Category category) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            updateCategory(con, category);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if(con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * Update category.
     *
     * @param category
     *            category to update.
     * @throws SQLException if insert not valid data
     */
    public void updateCategory(Connection con, Category category) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL_UPDATE_CATEGORY);
        int k = 1;
        pstmt.setString(k++, category.getNameUa());
        pstmt.setString(k++, category.getNameEn());
        pstmt.setString(k++, category.getNameRu());
        pstmt.setLong(k, category.getId());
        pstmt.executeUpdate();
        pstmt.close();
    }
    /**
     * Insert category.
     *
     * @param category
     *            personal to insert.
     */
    public void insertCategory (Category category) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            insertCategory(con, category);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if(con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
    }
    /**
     * Insert category.
     *
     * @param category
     *            category to insert.
     * @throws SQLException if insert not valid data
     */
    public void insertCategory(Connection con, Category category) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL_INSERT_CATEGORY);
        int k = 1;
        pstmt.setString(k++, category.getNameUa());
        pstmt.setString(k++, category.getNameEn());
        pstmt.setString(k, category.getNameRu());
        pstmt.executeUpdate();
        pstmt.close();
    }

    /**
     * Deletes category by identifier
     * @param id category identifier
     * @throws SQLRestrictException if database restrict deleting category related to hospital staff
     */
    public void deleteCategory(Long id) throws SQLRestrictException {
        PreparedStatement pstmt;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_DELETE_CATEGORY);
            pstmt.setLong(1, id);
            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            throw new SQLRestrictException("It is not possible to delete a category related to hospital staff", ex);
        } finally {
            if(con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * Extracts a category from the result set row.
     */
    private static class CategoryMapper implements EntityMapper<Category> {

        @Override
        public Category mapRow(ResultSet rs) {
            try {
                Category category = new Category();
                category.setId(rs.getLong(Fields.ENTITY_ID));
                category.setNameUa(rs.getString(Fields.CATEGORY_NAME_UA));
                category.setNameEn(rs.getString(Fields.CATEGORY_NAME_EN));
                category.setNameRu(rs.getString(Fields.CATEGORY_NAME_RU));
                return category;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
