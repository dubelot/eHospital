package com.epam.learn.odubelt.hospital.web.filter;

import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.web.Parameters;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

/**
 * Security filter. Disabled by default. Uncomment Security filter
 * section in web.xml to enable.
 * 
 * @author O.Dubelt
 * 
 */
public class CommandAccessFilter implements Filter {

	private static final Logger log = Logger.getLogger(CommandAccessFilter.class);

	public static final String OUT_OF_CONTROL = "out-of-control";
	public static final String COMMON = "common";

	// commands access	
	private static final Map<String, List<String>> accessMap = new HashMap<>();
	private static List<String> commons = new ArrayList<>();
	private static List<String> outOfControl = new ArrayList<>();
	
	public void destroy() {
		log.debug("Filter destruction starts");
		// do nothing
		log.debug("Filter destruction finished");
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		log.debug("Filter starts");
		
		if (accessAllowed(request)) {
			log.debug("Filter finished");
			chain.doFilter(request, response);
		} else {
			String locale = (String) request.getAttribute(Parameters.DEFAULT_LOCALE);
			if (locale==null) locale = Parameters.UA;
			ResourceBundle bundle = ResourceBundle.getBundle(Parameters.RESOURCES, Locale.forLanguageTag(locale));
			String errorMessage = bundle.getString("filter.access.no_permission");
			
			request.setAttribute(Parameters.ERROR_MESSAGE, errorMessage);
			log.trace("Set the request attribute: errorMessage --> " + errorMessage);
			
			request.getRequestDispatcher(Path.PAGE_ERROR_PAGE)
					.forward(request, response);
		}
	}
	
	private boolean accessAllowed(ServletRequest request) {
			HttpServletRequest httpRequest = (HttpServletRequest) request;

		String commandName = request.getParameter(Parameters.COMMAND);
		if (commandName == null || commandName.isEmpty())
			return false;
		
		if (outOfControl.contains(commandName))
			return true;
		
		HttpSession session = httpRequest.getSession(false);
		if (session == null) 
			return false;
		
		String userType = (String) session.getAttribute(Parameters.USER_TYPE);
		if (userType == null)
			return false;
		
		return accessMap.get(userType).contains(commandName)
				|| commons.contains(commandName);
	}

	public void init(FilterConfig fConfig) {
		log.debug("Filter initialization starts");
		
		// types
		accessMap.put(Parameters.ADMIN, asList(fConfig.getInitParameter(Parameters.ADMIN)));
		accessMap.put(Parameters.PATIENT, asList(fConfig.getInitParameter(Parameters.PATIENT)));
		accessMap.put(Parameters.PERSONAL, asList(fConfig.getInitParameter(Parameters.PERSONAL)));
		log.trace("Access map --> " + accessMap);

		// commons
		commons = asList(fConfig.getInitParameter(COMMON));
		log.trace("Common commands --> " + commons);

		// out of control
		outOfControl = asList(fConfig.getInitParameter(OUT_OF_CONTROL));
		log.trace("Out of control commands --> " + outOfControl);
		
		log.debug("Filter initialization finished");
	}
	
	/**
	 * Extracts parameter values from string.
	 * 
	 * @param str
	 *            parameter values string.
	 * @return list of parameter values.
	 */
	private List<String> asList(String str) {
		List<String> list = new ArrayList<>();
		StringTokenizer st = new StringTokenizer(str);
		while (st.hasMoreTokens()) list.add(st.nextToken());
		return list;		
	}
	
}