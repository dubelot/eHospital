package com.epam.learn.odubelt.hospital.web.command.admin;

import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.PatientDao;
import com.epam.learn.odubelt.hospital.db.PersonalDao;
import com.epam.learn.odubelt.hospital.db.entity.Patient;
import com.epam.learn.odubelt.hospital.db.entity.Personal;
import com.epam.learn.odubelt.hospital.web.command.Command;
import com.epam.learn.odubelt.hospital.web.command.CommonCommands;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
/**
 * Command for open form for appointing doctor to a patient.
 *
 * @author O.Dubelt
 *
 */
public class FormAppointDoctorCommand extends Command {
    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger log = Logger.getLogger(FormAppointDoctorCommand.class);
    public static final String DOCTOR_BY_CATEGORY_LIST = "doctorByCategoryList";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.debug("Command starts");
        Long id = Long.parseLong(request.getParameter(Parameters.ID));
        Patient patient = new PatientDao().findPatient(id);
        request.setAttribute(Parameters.PATIENT, patient);
        log.trace("Set the request attribute: patient --> " + patient);

        CommonCommands.setPersonalAndCategoriesForPatient(log, request, id);

        String categoryId = request.getParameter(Parameters.CATEGORY_ID);
        if (categoryId!=null && !categoryId.isEmpty()) {
            Long catId = Long.parseLong(categoryId);
            request.setAttribute(Parameters.CATEGORY_ID, catId);
            log.trace("Set the request attribute: categoryId --> " + catId);

            List<Personal> doctorByCategoryList = new PersonalDao().getDoctorListByCategory(catId);
            request.setAttribute(DOCTOR_BY_CATEGORY_LIST, doctorByCategoryList);
            log.trace("Set the request attribute: doctorByCategoryList --> " + doctorByCategoryList);
        }
        log.debug("Command finished");

        return Path.PAGE_APPOINT;
    }
}
