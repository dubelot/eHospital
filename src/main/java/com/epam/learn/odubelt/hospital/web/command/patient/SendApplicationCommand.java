package com.epam.learn.odubelt.hospital.web.command.patient;

import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.PatientDao;
import com.epam.learn.odubelt.hospital.db.entity.Patient;
import com.epam.learn.odubelt.hospital.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Command for sending application to a hospital
 *
 * @author O.Dubelt
 *
 */
public class SendApplicationCommand extends Command {
    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger log = Logger.getLogger(SendApplicationCommand.class);
    public static final String APPLICATION = "application";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.debug("Command starts");

        String application = request.getParameter(APPLICATION);
        log.trace("Get the request attribute: application --> " + application);

        Patient patient = (Patient) request.getSession().getAttribute(Parameters.USER);
        patient.setApplication(application);
        new PatientDao().updatePatientApplication(patient.getId(), application);

        log.debug("Command finished");
        return Path.COMMAND_SUCCESS;
    }
}
