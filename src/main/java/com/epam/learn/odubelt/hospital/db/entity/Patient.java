package com.epam.learn.odubelt.hospital.db.entity;

import javax.persistence.Entity;
import java.sql.Date;

/**
 * Patient entity.
 * 
 * @author O.Dubelt
 * 
 */
@Entity
public class Patient extends User {

	private static final long serialVersionUID = 3119185193042693050L;

	private Date birthday;
	private Long medCardId;
	private String application;

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Long getMedCardId() {
		return medCardId;
	}

	public void setMedCardId(Long medCardId) {
		this.medCardId = medCardId;
	}

	public String getApplication() {
		return application;
	}
	public void setApplication(String application) {
		this.application = application;
	}

	@Override
	public String toString() {
		return "Personal [id=" + getId()+ ", login=" + getLogin() + ", password=" + getPassword()
				+ ", firstName=" + getFirstName() + ", lastName=" + getLastName()+", idNumber=" + getIdNumber()
				+ ", locale=" + getLocale() + ", birthday="+ birthday + ", medCardId="+ medCardId + ", application="+ application+']';
	}

}
