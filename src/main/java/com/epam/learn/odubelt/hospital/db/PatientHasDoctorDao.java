package com.epam.learn.odubelt.hospital.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
/**
 * Data access object for Patient-Doctor relations.
 */
public class PatientHasDoctorDao {
    public static final String SQL_INSERT_PATIENT_HAS_DOCTOR ="INSERT INTO patient_has_doctor VALUES(?,?,?)";
    public static final String SQL_DELETE_PATIENT_HAS_DOCTOR ="DELETE FROM patient_has_doctor WHERE patient_id=? AND doctor_id=?";

    /**
     * Inserts patient-doctor relation
     * @param patientId patient identifier
     * @param personalId personal identifier
     * @param adminId admin identifier
     */
    public void insertEntry (Long patientId, Long personalId, Long adminId) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PreparedStatement pstmt = con.prepareStatement(SQL_INSERT_PATIENT_HAS_DOCTOR);
            int k = 1;
            pstmt.setLong(k++, patientId);
            pstmt.setLong(k++, personalId);
            pstmt.setLong(k, adminId);
            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            throw new RuntimeException(ex);
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
    }

    /**
     * Deletes patient-doctor relation
     * @param patientId patient identifier
     * @param personalId personal identifier
     */
    public void deleteEntry (Long patientId, Long personalId) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PreparedStatement pstmt = con.prepareStatement(SQL_DELETE_PATIENT_HAS_DOCTOR);
            int k = 1;
            pstmt.setLong(k++, patientId);
            pstmt.setLong(k, personalId);
            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            throw new RuntimeException(ex);
        } finally {
            if (con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
    }

}
