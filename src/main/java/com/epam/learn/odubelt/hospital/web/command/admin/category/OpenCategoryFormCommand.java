package com.epam.learn.odubelt.hospital.web.command.admin.category;

import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Command for open form for adding new category.
 *
 * @author O.Dubelt
 *
 */
public class OpenCategoryFormCommand extends Command {
    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger log = Logger.getLogger(OpenCategoryFormCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.debug("Command starts");

        log.debug("Command finished");
        return Path.PAGE_FORM_CATEGORY;
    }
}
