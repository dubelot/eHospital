package com.epam.learn.odubelt.hospital.db;

import com.epam.learn.odubelt.hospital.db.entity.CardEntry;
import java.sql.*;
import java.util.ArrayList;

/**
 * Data access object for MedCard, CardEntry & Prescription entities.
 */
public class MedCardEntriesDAO {
    private static final String SQL_INSERT_MED_CARD =
            "INSERT INTO med_card VALUES()";
    private static final String SQL_SELECT_LAST_MED_CARD = "SELECT * FROM med_card ORDER BY id DESC LIMIT 1";
    private static final String SQL_INSERT_CARD_ENTRY =
            "INSERT INTO card_entry (personal_id, entry_type_id, med_card_id, symptoms, note, diagnosis, prescription_id) " +
                    "VALUES (?,?,?,?,?,?,?)";
    private static final String SQL_LIST_ENTRY_SIZE =
            "SELECT COUNT(id) FROM card_entry WHERE med_card_id=?";
    private static final String SQL_FIND_ENTRY_LIST =
            "SELECT * FROM card_entry WHERE med_card_id=? ORDER BY create_time LIMIT ?,?";
    private static final String SQL_FIND_ENTRY_BY_ID = "SELECT * FROM card_entry WHERE id=?";

    /**
     * Create medCard.
     *
     * @return id of created MedCard
     *
     */
    public Long createMedCard () {
        Long id = null;
        Connection con = null;
        Statement stmt;
        ResultSet rs;
        try {
            con = DBManager.getInstance().getConnection();
            stmt = con.createStatement();
            stmt.executeUpdate(SQL_INSERT_MED_CARD);
            rs = stmt.executeQuery(SQL_SELECT_LAST_MED_CARD);
            if (rs.next()) {
                id = rs.getLong(Fields.ENTITY_ID);
            }
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if(con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
        return id;
    }
    /**
     * Counts all card entries for given Medical card identifier in database
     * @param medCardId medical card identifier
     * @return size of card entries list for given Medical card identifier
     */
    public int listSize(Long medCardId) {
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        int size = 0;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_LIST_ENTRY_SIZE);
            pstmt.setLong(1, medCardId);
            rs = pstmt.executeQuery();
            rs.next();
            size = rs.getInt(1);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if(con!=null)
                DBManager.getInstance().commitAndClose(con);
        }
        return size;
    }

    /**
     * Gets all entries for given medical card identifier
     * @param medCardId medical card identifier
     * @param currentPage number of page
     * @param desc order of list
     * @return list of card entries
     */
    public ArrayList<CardEntry> getEntryList(Long medCardId, int currentPage, boolean desc) {
        ArrayList<CardEntry> list = new ArrayList<>();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            String sql = SQL_FIND_ENTRY_LIST;
            if (desc) {
                StringBuilder sbSql = new StringBuilder(sql);
                int offset = sbSql.lastIndexOf(Fields.ENTRY_CREATE_TIME) + Fields.ENTRY_CREATE_TIME.length();
                sbSql.insert(offset, " DESC");
                sql = sbSql.toString();
            }
            con = DBManager.getInstance().getConnection();
            CardEntryMapper mapper = new CardEntryMapper();
            pstmt = con.prepareStatement(sql);
            int k = 1;
            pstmt.setLong(k++, medCardId);
            pstmt.setInt(k++, (currentPage - 1) * 3);
            pstmt.setInt(k, currentPage * 3);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(mapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if(con!=null)
                DBManager.getInstance().commitAndClose(con);
        }
        return list;
    }

    /**
     * Finds card entry with given identifier
     * @param id card entry identifier
     * @return card entry
     */
    public CardEntry findCardEntry(Long id) {
        CardEntry entry = new CardEntry();
        PreparedStatement pstmt;
        ResultSet rs;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            CardEntryMapper mapper = new CardEntryMapper();
            pstmt = con.prepareStatement(SQL_FIND_ENTRY_BY_ID);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                entry = mapper.mapRow(rs);
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if(con!=null)
                DBManager.getInstance().commitAndClose(con);
        }
        return entry;
    }
    /**
     * Inserts new card entry to database
     * @param cardEntry to insert
     */
    public void insertCardEntry(CardEntry cardEntry) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            insertCardEntry(con, cardEntry);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            if(con!=null)
            DBManager.getInstance().commitAndClose(con);
        }
    }
    /**
     *  Inserts new card entry to database
     *
     * @param cardEntry to insert
     * @throws SQLException if insert not valid data
     */
    private void insertCardEntry(Connection con, CardEntry cardEntry) throws SQLException {
        PreparedStatement pstmt = con.prepareStatement(SQL_INSERT_CARD_ENTRY);
        int k = 1;
        pstmt.setLong(k++, cardEntry.getPersonalId());
        pstmt.setInt(k++, cardEntry.getEntryTypeId());
        pstmt.setLong(k++, cardEntry.getMedCardId());
        pstmt.setString(k++, cardEntry.getSymptoms());
        pstmt.setString(k++, cardEntry.getNote());
        pstmt.setString(k++, cardEntry.getDiagnosis());
        Long prescriptionId = cardEntry.getPrescriptionId();
        if (prescriptionId==null){
            pstmt.setObject(k, prescriptionId, Types.LONGVARBINARY);
        } else {
            pstmt.setLong(k, prescriptionId);
        }
        pstmt.executeUpdate();
        pstmt.close();
    }

    /**
     * Extracts a card entry from the result set row.
     */

    private static class CardEntryMapper implements EntityMapper<CardEntry> {
        @Override
        public CardEntry mapRow(ResultSet rs) {
            try {
                CardEntry cardEntry = new CardEntry();
                cardEntry.setId(rs.getLong(Fields.ENTITY_ID));
                cardEntry.setCreateTime(rs.getDate(Fields.ENTRY_CREATE_TIME));
                cardEntry.setPersonalId(rs.getLong(Fields.ENTRY_PERSONAL_ID));
                cardEntry.setEntryTypeId(rs.getInt(Fields.ENTRY_ENTRY_TYPE_ID));
                cardEntry.setMedCardId(rs.getLong(Fields.ENTRY_MED_CARD_ID));
                cardEntry.setSymptoms(rs.getString(Fields.ENTRY_SYMPTOMS));
                cardEntry.setNote(rs.getString(Fields.ENTRY_NOTE));
                cardEntry.setDiagnosis(rs.getString(Fields.ENTRY_DIAGNOSIS));
                cardEntry.setPrescriptionId(rs.getObject(Fields.ENTRY_PRESCRIPTION_ID, Long.TYPE));
                return cardEntry;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
