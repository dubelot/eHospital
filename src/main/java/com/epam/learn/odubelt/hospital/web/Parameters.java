package com.epam.learn.odubelt.hospital.web;
/**
 * Holder for parameter and attribute names of servlet.
 *
 * @author O.Dubelt
 *
 */
public final class Parameters {
    public static final String USER = "user";
    public static final String ADMIN = "admin";
    public static final String PATIENT = "patient";
    public static final String PERSONAL = "personal";
    public static final String DOCTOR = "doctor";
    public static final String NURSE = "nurse";
    public static final String CATEGORY = "category";
    public static final String PRESCRIPTION = "prescription";

    public static final String ID = "id";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String ID_NUMBER = "idNumber";
    public static final String BIRTHDAY = "birthday";
    public static final String CATEGORY_ID = "categoryId";
    public static final String ROLE_ID = "roleId";
    public static final String NURSE_ID = "nurseId";

    public static final String PATIENT_ID = "patientId";
    public static final String PERSONAL_ID = "personalId";

    public static final String NAME_UA = "nameUa";
    public static final String NAME_EN = "nameEn";
    public static final String NAME_RU = "nameRu";

    public static final String ENTRY_TYPE_ID = "entryTypeId";
    public static final String MED_CARD_ID = "medCardId";
    public static final String SYMPTOMS = "symptoms";
    public static final String NOTE = "note";
    public static final String DIAGNOSIS = "diagnosis";
    public static final String MEDICINE = "medicine";
    public static final String PROCEDURE = "procedure";
    public static final String SURGERY = "surgery";

    public static final String COMMAND = "command";
    public static final String RESOURCES = "resources";

    public static final String ENTRY_LIST = "entryList";
    public static final String CATEGORY_LIST = "categoryList";
    public static final String PERSONAL_LIST = "personalList";
    public static final String NURSE_LIST = "nurseList";
    public static final String ADMIN_LIST = "adminList";
    public static final String PATIENT_LIST = "patientList";

    public static final String NAME = "name";
    public static final String TYPE = "type";
    public static final String VALUE = "value";
    public static final String COUNT_PERSONAL = "countPersonal";
    public static final String USER_TYPE = "userType";
    public static final String ORDER_BY = "orderBy";
    public static final String DESC = "desc";
    public static final String CURRENT_PAGE = "currentPage";
    public static final String COUNT_PAGE = "countPage";
    public static final String ERROR_MESSAGE = "errorMessage";
    public static final String DEFAULT_LOCALE = "defaultLocale";
    public static final String LOCALE_TO_SET = "localeToSet";
    public static final String JAVAX_SERVLET_JSP_JSTL_FMT_LOCALE = "javax.servlet.jsp.jstl.fmt.locale";
    public static final String UA = "ua";
    public static final String EN = "en";
    public static final String RU = "ru";


}
