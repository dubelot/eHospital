package com.epam.learn.odubelt.hospital.web.command.admin.category;

import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.db.CategoryDao;
import com.epam.learn.odubelt.hospital.db.entity.Category;
import com.epam.learn.odubelt.hospital.web.command.Command;
import com.epam.learn.odubelt.hospital.web.Parameters;
import com.epam.learn.odubelt.hospital.web.command.ValidationException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Command for editing category.
 *
 * @author O.Dubelt
 *
 */
public class EditCategoryCommand extends Command {
    private static final long serialVersionUID = -3071536593627692473L;
    private static final Logger log = Logger.getLogger(EditCategoryCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        log.debug("Command starts");

        Long id = Long.parseLong(request.getParameter(Parameters.ID));
        Category category = new CategoryDao().findCategory(id);
        try {
            new CategoryUpdater().updateCategoryFromRequest(request, category);
        } catch (ValidationException e) {
            request.setAttribute("id", id);
            request.setAttribute(Parameters.ERROR_MESSAGE, e.getMessage());
            log.trace("Validation error --> " + e.getMessage());
            return Path.COMMAND_OPEN_EDIT_CATEGORY_FORM;
        }
        //update category in db
        new CategoryDao().updateCategory(category);
        log.trace("Insert category to db --> " + category);

        log.debug("Command finished");
        return Path.COMMAND_SUCCESS;
    }
}
