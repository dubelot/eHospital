<%@ include file="WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title" value="Login"/>
<%@ include file="WEB-INF/jspf/head.jspf" %>
	
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="container-md p-5">
	<div class="row">
	<div class="col-md-3">
	</div>
<div class="col-md-6">
	<h2 class="text-center"><fmt:message key="login_jsp.button.login"/></h2>
	<form action="controller" method="post">
		<input type="hidden" name="command" value="login"/>
		<div class="form-group">
			<label for="login"><fmt:message key="login_jsp.login"/></label>
			<input type="text" class="form-control" id="login" placeholder="<fmt:message key="login_jsp.enter_login"/>" name="login" required>
		</div>
		<div class="form-group">
			<label for="pwd"><fmt:message key="login_jsp.label.password"/></label>
			<input type="password" class="form-control" id="pwd" placeholder="<fmt:message key="login_jsp.enter_password"/>" name="password" required>
		</div>
		<button type="submit" class="btn btn-primary"><fmt:message key="login_jsp.button.login"/></button>
	</form>
</div>
	<div class="col-md-3">
	</div>
	</div>
</div>
		<%@ include file="WEB-INF/jspf/footer.jspf"%>
</body>
</html>