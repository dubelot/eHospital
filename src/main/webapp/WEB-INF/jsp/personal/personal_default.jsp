<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="Form personal" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="container-fluid">
	<div class="row flex-xl-nowrap">
		<%@ include file="/WEB-INF/jspf/menu.jspf" %>
		<div class="container p-3 mx-auto">
			<div class="form-row">
				<div class="col-md-9 mb-3">
				<h6 class="font-weight-bold"><fmt:message key="form_appoint_jsp.appointed_patients"/>:</h6>
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-9 mb-3">
					<table class="table .table-sm table-striped table-responsive">
						<thead>
						<tr>
							<td>№</td>
							<td><fmt:message key="list_patient_jsp.table.header.patient"/></td>
							<td><fmt:message key="list_personal_jsp.table.header.id_number"/></td>
							<td><fmt:message key="list_patient_jsp.table.header.birthday"/></td>
							<td><fmt:message key="list_patient_jsp.table.header.medcard"/></td>
						</tr>
						</thead>
						<c:forEach var="bean" items="${patientList}">
							<tr>
								<td>${i=i+1}</td>
								<td><a href="controller?command=openEntryForm&id=${bean.id}">${bean.lastName} ${bean.firstName}</a></td>
								<td>${bean.idNumber}</td>
								<td>${bean.birthday}</td>
								<td class="text-center"><a href="controller?command=viewMedCard&id=${bean.medCardId}"><fmt:message key="personal_default_jsp.table.view"/></a></td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>
	</div>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>