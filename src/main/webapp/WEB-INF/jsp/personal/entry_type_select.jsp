<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="Form personal" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="container-fluid">
	<div class="row flex-xl-nowrap">
		<%@ include file="/WEB-INF/jspf/menu.jspf" %>
		<div class="container p-3 mx-auto">
			<div class="form-row">
				<div class="col-md-9 mb-3">
				<h6 class="font-weight-bold"><fmt:message key="form_appoint_jsp.select_entry_type"/>:</h6>
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-9 mb-3">
					<div class="list-group">
						<a href="controller?command=openEntryForm&type=appointment&id=${id}" class="list-group-item list-group-item-action"><fmt:message key="jsp_entity.entry_type.appointment"/></a>
						<a href="controller?command=openEntryForm&type=procedure&id=${id}" class="list-group-item list-group-item-action"><fmt:message key="jsp_entity.entry_type.procedure"/></a>
						<a href="controller?command=openEntryForm&type=discharge&id=${id}" class="list-group-item list-group-item-action"><fmt:message key="jsp_entity.entry_type.discharge"/></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>