<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="Entry form" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="container-fluid">
	<div class="row flex-xl-nowrap">
		<%@ include file="/WEB-INF/jspf/menu.jspf" %>
		<div class="container p-3 mx-auto">
			<form action="controller" method="post">
				<input type="hidden" name="command" value="addCardEntry">
				<input type="hidden" name="medCardId" value="${patient.medCardId}">
			<div class="form-row">
				<div class="col-md-12 mb-3 text-center">
				<h4 class="font-weight-bold text-capitalize">
					<c:choose>
						<c:when test="${type=='appointment'}">
							<input type="hidden" name="entryTypeId" value="1">
							<fmt:message key="jsp_entity.entry_type.appointment"/>
						</c:when>
						<c:when test="${type=='procedure'}">
							<input type="hidden" name="entryTypeId" value="2">
							<fmt:message key="jsp_entity.entry_type.procedure"/>
						</c:when>
						<c:otherwise>
							<input type="hidden" name="entryTypeId" value="3">
							<fmt:message key="jsp_entity.entry_type.discharge"/>
						</c:otherwise>
					</c:choose>
				</h4>
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-3 mb-3">
					<p class="font-weight-bold"><fmt:message key="jsp_entity_type.patient"/></p>
				</div>
				<div class="col-md-3 mb-3">
					<p class="font-italic">${patient.firstName} ${patient.lastName}</p>
				</div>
				<div class="col-md-3 mb-3">
					<p class="font-weight-bold"><fmt:message key="form_user_jsp.date_of_birth"/></p>
				</div>
				<div class="col-md-3 mb-3">
					<p class="font-italic">${patient.birthday}</p>
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-3 mb-3">
					<p class="font-weight-bold"><fmt:message key="jsp_entity_type.med_card"/></p>
				</div>
				<div class="col-md-3 mb-3">
					<p class="font-italic">${patient.medCardId}</p>
				</div>
				<div class="col-md-3 mb-3">
					<p class="font-weight-bold"><fmt:message key="jsp_entity_type.user.locale"/></p>
				</div>
				<div class="col-md-3 mb-3">
					<p class="font-italic">${patient.locale}</p>
				</div>
			</div>
				<div class="form-row">
					<div class="col-md-3 mb-3">
						<p class="font-weight-bold"><fmt:message key="jsp_entity_type.card_entry.symptoms"/></p>
					</div>
					<div class="col-md-9 mb-3">
						<textarea  class="form-control" type="text" name ="symptoms" id="textarea1" rows="3" placeholder="<fmt:message key="patient_default_jsp.enter_symptoms"/>"></textarea>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-3 mb-3">
						<p class="font-weight-bold"><fmt:message key="jsp_entity_type.card_entry.note"/></p>
					</div>
					<div class="col-md-9 mb-3">
						<textarea  class="form-control" type="text" name ="note" id="textarea2" rows="3" placeholder="<fmt:message key="patient_default_jsp.enter_notes"/>"></textarea>
					</div>
				</div>
				<c:if test="${user.roleId=='1'}">
				<div class="form-row">
					<c:choose>
						<c:when test="${type=='discharge'}">
							<div class="col-md-3 mb-3">
								<p class="font-weight-bold"><fmt:message key="jsp_entity_type.card_entry.diagnosis"/></p>
							</div>
						</c:when>
						<c:otherwise>
							<div class="col-md-3 mb-3">
								<p class="font-weight-bold"><fmt:message key="jsp_entity_type.card_entry.pre_diagnosis"/></p>
							</div>
						</c:otherwise>
					</c:choose>
					<div class="col-md-9 mb-3">
						<textarea  class="form-control" type="text" maxlength="255" name ="diagnosis" id="textarea3" rows="3" placeholder="<fmt:message key="patient_default_jsp.enter_diagnosis"/>"></textarea>
					</div>
				</div>
				</c:if>
				<div class="form-row">
					<div class="col-md-12 mb-3 text-center">
						<h6 class="font-weight-bold">
							<fmt:message key="jsp_entity.entry_type.prescription"/>
						</h6>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-3 mb-3">
						<p class="font-weight-bold"><fmt:message key="jsp_entity_type.prescription.medicine"/></p>
					</div>
					<div class="col-md-9 mb-3">
						<textarea  class="form-control" type="text" maxlength="255" name ="medicine" id="textarea4" rows="3" placeholder="<fmt:message key="patient_default_jsp.enter_medicines"/>"></textarea>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-3 mb-3">
						<p class="font-weight-bold"><fmt:message key="jsp_entity_type.prescription.procedure"/></p>
					</div>
					<div class="col-md-9 mb-3">
						<textarea  class="form-control" type="text" maxlength="255" name ="procedure" id="textarea5" rows="3" placeholder="<fmt:message key="patient_default_jsp.enter_procedures"/>"></textarea>
					</div>
				</div>
				<c:if test="${user.roleId=='1'}">
					<div class="form-row">
						<div class="col-md-3 mb-3">
							<p class="font-weight-bold"><fmt:message key="jsp_entity_type.prescription.surgery"/></p>
						</div>
						<div class="col-md-9 mb-3">
							<textarea  class="form-control" type="text" maxlength="255" name ="surgery" id="textarea6" rows="3" placeholder="<fmt:message key="patient_default_jsp.enter_surgeries"/>"></textarea>
						</div>
					</div>
				</c:if>
				<div class="form-row">
					<div class="col-md-3 mb-3">
						<button class="btn btn-primary" type="submit"><fmt:message key="entry_form_jsp.make_entry"/></button>
					</div>
				</div>
			</form>
	</div>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>