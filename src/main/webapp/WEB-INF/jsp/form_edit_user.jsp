<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="Form edit user" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="container-fluid">
	<div class="row flex-xl-nowrap">
		<%@ include file="/WEB-INF/jspf/menu.jspf" %>

		<div class="container p-3 mx-auto">
			<form action="controller?command=editUser&editType=${editType}" method="post">
				<input type="hidden" name="id" id="validationDefault00" value="${userEdit.id}">
				<c:if test="${editType=='personal'}">
				<input type="hidden" name="roleId" id="validationDefault00" value="${userEdit.roleId}">
				</c:if>
				<div class="form-row">
					<div class="col-md-10 mb-3">
						<p class="text-danger">${errorMessage}</p>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6 mb-3">
						<label for="validationDefault01"><fmt:message key="form_user_jsp.first_name"/></label>
						<input type="text" class="form-control" name="firstName" id="validationDefault01" value="${userEdit.firstName}" required>
					</div>
					<div class="col-md-6 mb-3">
						<label for="validationDefault02"><fmt:message key="form_user_jsp.last_name"/></label>
						<input type="text" class="form-control" name="lastName" id="validationDefault02" value="${userEdit.lastName}" required>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6 mb-3">
						<label for="validationDefault03"><fmt:message key="form_user_jsp.login"/></label>
						<input type="text" class="form-control" name="login" id="validationDefault03" value="${userEdit.login}"  disabled>
					</div>
					<div class="col-md-6 mb-3">
						<label for="validationDefault04"><fmt:message key="form_user_jsp.password"/></label>
						<input type="password" class="form-control" name="password" id="validationDefault04" value="${userEdit.password}" disabled>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6 mb-3">
						<label for="validationDefault05"><fmt:message key="form_user_jsp.id_number"/></label>
						<input type="text" maxlength="10" class="form-control" name="idNumber" id="validationDefault05" value="${userEdit.idNumber}" required>
						<small class="form-text text-muted"><fmt:message key="regex.user.id_number"/></small>
					</div>
					<div class="col-md-6 mb-3">
					<label for="validationDefault06"><fmt:message key="form_user_jsp.date_of_birth"/></label>
					<c:choose>
						<c:when test="${editType=='patient'}">
							<input type="date" class="form-control" name="birthday" id="validationDefault06" value="${userEdit.birthday}" required>
						</c:when>
						<c:otherwise>
							<input type="date" class="form-control" name="birthday" id="validationDefault06" disabled>
						</c:otherwise>
					</c:choose>
					</div>
				</div>
				<c:if test="${editType=='personal'}">
				<div class="form-row">
					<div class="col-md-6 mb-3">
						<label for="validationDefault07"><fmt:message key="form_user_jsp.category"/></label>
						<select class="custom-select" name="categoryId" id="validationDefault07" required>
							<option selected disabled value="${categoryList.get(userEdit.categoryId)}">
								<my:category categoryId="${userEdit.categoryId}">
									${pageContext.getAttribute("value")}
								</my:category>
							</option>
							<c:forEach var="bean" items="${categoryList}">
							<my:category categoryId="${bean.id}">
								<option value="${bean.id}">${pageContext.getAttribute("value")}</option>
							</my:category>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6 mb-3">
						<label for="validationDefault08"><fmt:message key="form_user_jsp.add_nurse"/></label>
						<c:choose>
							<c:when test="${userEdit.roleId=='1'}">
								<select class="custom-select" name="nurseId" id="validationDefault08">
									<c:choose>
										<c:when test="${userEdit.nurseId eq null}">
											<option selected disabled value=""><--null--></option>
										</c:when>
										<c:otherwise>
											<option selected disabled value="${userEdit.nurseId}">
												<my:personal personalId="${userEdit.nurseId}">${personal.lastName} ${personal.firstName} ${personal.idNumber}
													<my:category categoryId="${personal.categoryId}">
													${pageContext.getAttribute("value")}
													</my:category>
												</my:personal>
											</option>
										</c:otherwise>
									</c:choose>
									<c:forEach var="bean" items="${nurseList}">
										<option value="${bean.id}">${bean.lastName} ${bean.firstName} ${bean.idNumber} <my:category categoryId="${bean.categoryId}">${pageContext.getAttribute("value")}</my:category></option>
									</c:forEach>
								</select>
							</c:when>
							<c:otherwise>
								<select class="custom-select" name="nurseId" id="validationDefault08" disabled>
									<option selected disabled value=""><fmt:message key="form_user_jsp.choose"/></option>
								</select>
							</c:otherwise>
						</c:choose>
					</div>
					<c:if test="${userEdit.nurseId ne null}">
					<div class="col-md-6 p-3">
						<a class="btn btn-primary m-3" href="controller?command=detachNurse&id=${userEdit.id}"><fmt:message key="form_appoint_jsp.detach"/></a>
					</div>
					</c:if>
				</div>
				</c:if>
				<button class="btn btn-primary" type="submit"><fmt:message key="settings_jsp.button.update"/></button>
			</form>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>