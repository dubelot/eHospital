<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>

<c:set var="title" value="Settings" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="container-fluid">
	<div class="container p-3 mx-auto">
		<form id="settings_form" action="controller" method="get">
			<input type="hidden" name="command" value="updateSettings" />
			<div class="form-row m-2">
				<div class="col-md-10 mb-3">
					<p class="text-danger">${errorMessage}</p>
				</div>
			</div>
			<div class="form-row m-2">
					<div class="col-3">
					<p class="px-3 m-2">
						<fmt:message key="settings_jsp.label.localization"/>
					</p>
					</div>
					<div class="col-3 py-2">
					<select name="localeToSet">
						<c:choose>
							<c:when test="${not empty defaultLocale}">
								<option value="">${defaultLocale}[Default]</option>
							</c:when>
							<c:otherwise>
								<option value=""></option>
							</c:otherwise>
						</c:choose>

						<c:forEach var="localeName" items="${locales}">
							<option value="${localeName}">${localeName}</option>
						</c:forEach>
					</select>
					</div>
				</div>
			<div class="form-row m-2">

				<p class="px-3 m-2 font-weight-bold">
					<fmt:message key="settings_jsp.label.change_password"/>
				</p>

			</div>
			<div class="form-row m-2">
				<div class="col-3">
				<p class="px-3 m-2">
					<fmt:message key="settings_jsp.label.old_password"/>
				</p>
				</div>
					<div class="col-3">
				<input type="password" name="oldPassword" placeholder="<fmt:message key="settings_jsp.label.enter_old_password"/>">
					</div>
					</div>
			<div class="form-row m-2">
				<div class="col-3">
				<p class="px-3 m-2">
					<fmt:message key="settings_jsp.label.new_password"/>
				</p>
				</div>
					<div class="col-3">
				<input type="password" name="newPassword" placeholder="<fmt:message key="settings_jsp.label.enter_new_password"/>">
					</div>
					</div>
			<button class="btn btn-primary m-3" type="submit"><fmt:message key="settings_jsp.button.update"/></button>

		</form>
	</div>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>