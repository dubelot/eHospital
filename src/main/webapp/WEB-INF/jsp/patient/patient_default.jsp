<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="Form personal" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="container-fluid">
	<div class="row flex-xl-nowrap">
		<%@ include file="/WEB-INF/jspf/menu.jspf" %>
		<div class="container p-3 mx-auto">
			<div class="form-row">
				<div class="col-md-6 mb-3">
					<h6 class="font-weight-bold"><fmt:message key="patient_default_jsp.send_application"/></h6>
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-6 mb-3">
					<form action="controller" method="post">
						<div class="form-group">
							<input type="hidden" name="command" value="sendApplication"/>
							<c:choose>
								<c:when test="${user.application eq null}">
									<textarea class="form-control" maxlength="255" type="text" name ="application" id="textarea1" rows="3" placeholder="<fmt:message key="patient_default_jsp.enter_application"/>"></textarea>
								</c:when>
								<c:otherwise>
									<textarea class="form-control" maxlength="255" type="text" name ="application" id="textarea1" rows="3">${user.application}</textarea>
									<small id="help" class="form-text text-muted"><fmt:message key="patient_default_jsp.application_tip"/></small>
								</c:otherwise>
							</c:choose>
							<button type="submit" class="btn btn-primary my-2"><fmt:message key="patient_default_jsp.send_application"/></button>
						</div>
					</form>
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-6 mb-3">
				<h6 class="font-weight-bold"><fmt:message key="form_appoint_jsp.appointed_doctors"/>:</h6>
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-6 mb-3">
				<table class="table .table-sm table-striped table-responsive">
					<thead>
					<tr>
						<td>№</td>
						<td><fmt:message key="list_personal_jsp.table.header.personal"/></td>
						<td><fmt:message key="list_personal_jsp.table.header.category"/></td>
					</tr>
					</thead>
					<c:set var="i" value="${0}" scope="page"/>
					<c:forEach var="bean" items="${personalList}">
						<tr>
							<td>${i=i+1}</td>
							<td>${bean.lastName} ${bean.firstName}</td>
							<my:category categoryId="${bean.categoryId}">
								<td>${pageContext.getAttribute("value")}</td>
							</my:category>
						</tr>
					</c:forEach>
				</table>
				</div>
			</div>
	</div>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>