<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="Form user" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="container-fluid">
	<div class="row flex-xl-nowrap">
		<%@ include file="/WEB-INF/jspf/menu.jspf" %>
		<div class="container p-3 mx-auto">
			<form action="controller?command=addUser&addType=${addType}" method="post">
				<div class="form-row">
					<div class="col-md-10 mb-3">
						<p class="text-danger">${errorMessage}</p>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6 mb-3">
						<label for="validationDefault01"><fmt:message key="form_user_jsp.first_name"/></label>
						<c:choose>
							<c:when test="${errorMessage ne null}">
								<input type="text" class="form-control" name="firstName" id="validationDefault01" value="${firstName}" required>
							</c:when>
							<c:otherwise>
								<input type="text" class="form-control" name="firstName" id="validationDefault01" placeholder="<fmt:message key="form_user_jsp.enter_first_name"/>" required>
							</c:otherwise>
						</c:choose>
					</div>
					<div class="col-md-6 mb-3">
						<label for="validationDefault02"><fmt:message key="form_user_jsp.last_name"/></label>
						<c:choose>
							<c:when test="${errorMessage ne null}">
								<input type="text" class="form-control" name="lastName" id="validationDefault02" value="${lastName}" required>
							</c:when>
							<c:otherwise>
								<input type="text" class="form-control" name="lastName" id="validationDefault02" placeholder="<fmt:message key="form_user_jsp.enter_last_name"/>" required>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6 mb-3">
						<label for="validationDefault03"><fmt:message key="form_user_jsp.login"/></label>
						<c:choose>
							<c:when test="${errorMessage ne null}">
								<input type="text" class="form-control" name="login" id="validationDefault03" value="${login}" required>
							</c:when>
							<c:otherwise>
								<input type="text" class="form-control" name="login" id="validationDefault03" placeholder="<fmt:message key="form_user_jsp.enter_login"/>" required>
							</c:otherwise>
						</c:choose>
						<small class="form-text text-muted"><fmt:message key="regex.user.login"/></small>
					</div>
					<div class="col-md-6 mb-3">
						<label for="validationDefault04"><fmt:message key="form_user_jsp.password"/></label>
						<input type="password" class="form-control" name="password" id="validationDefault04" placeholder="<fmt:message key="form_user_jsp.enter_temporary_password"/>" required>
						<small class="form-text text-muted"><fmt:message key="regex.user.password"/></small>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6 mb-3">
						<label for="validationDefault05"><fmt:message key="form_user_jsp.id_number"/></label>
						<c:choose>
							<c:when test="${errorMessage ne null}">
								<input type="text" maxlength="10" class="form-control" name="idNumber" id="validationDefault05" value="${idNumber}" required>
							</c:when>
							<c:otherwise>
								<input type="text" maxlength="10" class="form-control" name="idNumber" id="validationDefault05" placeholder="<fmt:message key="form_user_jsp.enter_id_number"/>" required>
							</c:otherwise>
						</c:choose>
						<small class="form-text text-muted"><fmt:message key="regex.user.id_number"/></small>
					</div>
					<div class="col-md-6 mb-3">
					<label for="validationDefault06"><fmt:message key="form_user_jsp.date_of_birth"/></label>
					<c:choose>
						<c:when test="${addType=='patient'}">
							<c:choose>
								<c:when test="${errorMessage ne null}">
									<input type="text" class="form-control" name="birthday" id="validationDefault06" value="${birthday}" required>
								</c:when>
								<c:otherwise>
									<input type="date" class="form-control" name="birthday" id="validationDefault06" required>
								</c:otherwise>
							</c:choose>

						</c:when>
						<c:otherwise>
							<input type="date" class="form-control" name="birthday" id="validationDefault06" disabled>
						</c:otherwise>
					</c:choose>
					</div>
				</div>
				<c:if test="${addType=='doctor' or addType=='nurse'}">

				<div class="form-row">
					<div class="col-md-6 mb-3">
						<label for="validationDefault07"><fmt:message key="form_user_jsp.category"/></label>
						<select class="custom-select" name="categoryId" id="validationDefault07" required>
							<option selected disabled value=""><fmt:message key="form_user_jsp.choose"/></option>
							<c:forEach var="bean" items="${categoryList}">
							<my:category categoryId="${bean.id}">
								<option value="${bean.id}"><p id="category1">${pageContext.getAttribute("value")}</p></option>
							</my:category>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6 mb-3">
						<label for="validationDefault08"><fmt:message key="form_user_jsp.add_nurse"/></label>
						<c:choose>
							<c:when test="${addType=='doctor'}">
								<select class="custom-select" name="nurseId" id="validationDefault08">
									<option selected disabled value=""><fmt:message key="form_user_jsp.choose"/></option>
									<c:forEach var="bean" items="${nurseList}">
										<option value="${bean.id}">${bean.lastName} ${bean.firstName} ${bean.idNumber} <my:category categoryId="${bean.categoryId}"><p id="category2">${pageContext.getAttribute("value")}</p></my:category></option>
									</c:forEach>
								</select>
							</c:when>
							<c:otherwise>
								<select class="custom-select" name="nurseId" id="validationDefault08" disabled>
									<option selected disabled value=""><fmt:message key="form_user_jsp.choose"/></option>
								</select>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				</c:if>
				<button class="btn btn-primary" type="submit"><fmt:message key="form_user_jsp.add"/></button>
				<p id="message"></p>
			</form>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>