<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="Entry form" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="container-fluid">
	<div class="row flex-xl-nowrap">
		<%@ include file="/WEB-INF/jspf/menu.jspf" %>
		<div class="container p-3 mx-auto">
			<div class="form-row">
				<div class="col-md-12 text-center">
					<h4 class="font-weight-bold"><fmt:message key="jsp_entity_type.med_card"/></h4>
					<c:choose>
						<c:when test="${!desc}">
							<a class="ml-3 my-3 float-right" href="controller?command=viewMedCard&id=${patient.id}&desc=true">
								<fmt:message key="med_card_jsp.date_desc"/>
							</a>
						</c:when>
						<c:when test="${desc}">
							<a class="ml-3 my-3 float-right" href="controller?command=viewMedCard&id=${patient.id}&desc=false">
								<fmt:message key="med_card_jsp.date_asc"/>
							</a>
						</c:when>
					</c:choose>
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-3">
					<p class="font-weight-bold"><fmt:message key="jsp_entity_type.patient"/></p>
				</div>
				<div class="col-md-3">
					<p class="font-italic">${patient.firstName} ${patient.lastName}</p>
				</div>
				<div class="col-md-3">
					<p class="font-weight-bold"><fmt:message key="form_user_jsp.date_of_birth"/></p>
				</div>
				<div class="col-md-3">
					<p class="font-italic">${patient.birthday}</p>
				</div>
			</div>
			<c:forEach var="bean" items="${entryList}">
				<div class="border border-dark my-2 p-2">
				<div class="form-row">
					<div class="col-md-12 text-center">
						<h5 class="font-weight-bold">
							<c:choose>
								<c:when test="${bean.entryTypeId=='1'}">
									<fmt:message key="jsp_entity.entry_type.appointment"/>
								</c:when>
								<c:when test="${bean.entryTypeId=='2'}">
									<fmt:message key="jsp_entity.entry_type.procedure"/>
								</c:when>
								<c:otherwise>
									<fmt:message key="jsp_entity.entry_type.discharge"/>
								</c:otherwise>
							</c:choose>     ${bean.createTime}</h5>
					</div>
				</div>
					<div class="form-row">
						<my:personal personalId="${bean.personalId}">
							<div class="col-md-3">
								<c:choose>
									<c:when test="${personal.roleId==1}">
										<p class="font-weight-bold"><fmt:message key="jsp_entity_type.doctor"/></p>
									</c:when>
									<c:otherwise>
										<p class="font-weight-bold"><fmt:message key="jsp_entity_type.nurse"/></p>
									</c:otherwise>
								</c:choose>
							</div>
							<div class="col-md-9">
								<my:category categoryId="${personal.categoryId}">
								<p class="font-italic">${pageContext.getAttribute("value")} ${personal.firstName} ${personal.lastName}</p>
								</my:category>
							</div>
						</my:personal>
					</div>
				<div class="form-row">
					<div class="col-md-3">
						<p class="font-weight-bold"><fmt:message key="jsp_entity_type.card_entry.symptoms"/></p>
					</div>
					<div class="col-md-9">
						<p class="font-italic">${bean.symptoms}</p>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-3">
						<p class="font-weight-bold"><fmt:message key="jsp_entity_type.card_entry.note"/></p>
					</div>
					<div class="col-md-9">
						<p class="font-italic">${bean.note}</p>
					</div>
				</div>
				<div class="form-row">
					<c:choose>
						<c:when test="${bean.entryTypeId=='3'}">
							<div class="col-md-3">
								<p class="font-weight-bold"><fmt:message key="jsp_entity_type.card_entry.diagnosis"/></p>
							</div>
						</c:when>
						<c:otherwise>
							<div class="col-md-3">
								<p class="font-weight-bold"><fmt:message key="jsp_entity_type.card_entry.pre_diagnosis"/></p>
							</div>
						</c:otherwise>
					</c:choose>
					<div class="col-md-9">
						<p class="font-italic">${bean.diagnosis}</p>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-12 text-center">
						<h6 class="font-weight-bold">
							<fmt:message key="jsp_entity.entry_type.prescription"/>
						</h6>
					</div>
				</div>
				<my:prescription prescriptionId="${bean.prescriptionId}">
				<div class="form-row">
					<div class="col-md-3">
						<p class="font-weight-bold"><fmt:message key="jsp_entity_type.prescription.medicine"/></p>
					</div>
					<div class="col-md-9">
						<p class="font-italic">${prescription.medicine}</p>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-3">
						<p class="font-weight-bold"><fmt:message key="jsp_entity_type.prescription.procedure"/></p>
					</div>
					<div class="col-md-9">
						<p class="font-italic">${prescription.procedure}</p>
					</div>
				</div>

				<div class="form-row">
					<div class="col-md-3">
						<p class="font-weight-bold"><fmt:message key="jsp_entity_type.prescription.surgery"/></p>
					</div>
					<div class="col-md-9">
						<p class="font-italic">${prescription.surgery}</p>
					</div>
				</div>
				</my:prescription>
					<div class="form-row">
						<div class="col-md-12 float-right">
							<a class="btn btn-primary" href="controller?command=savePDF&id=${bean.id}"><fmt:message key="med_card_jsp.button.save"/> </a>
						</div>
					</div>
				</div>

			</c:forEach>
			<nav aria-label="Page navigation example">
				<ul class="pagination justify-content-start">
					<c:choose>
						<c:when test="${currentPage eq 1}">
							<li class="page-item disabled">
								<a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
							</li>
						</c:when>
						<c:otherwise>
							<li class="page-item">
								<a class="page-link" href="controller?command=viewMedCard&id=${patient.id}&currentPage=${currentPage-1}&desc=${desc}">Previous</a>
							</li>
						</c:otherwise>
					</c:choose>
					<c:forEach var="i" begin="1" end="${countPage}">
						<c:choose>
							<c:when test="${currentPage eq i}">
								<li class="page-item active"><a class="page-link" href="controller?command=viewMedCard&id=${patient.id}&currentPage=${i}&desc=${desc}">${i}</a></li>
							</c:when>
							<c:otherwise>
								<li class="page-item"><a class="page-link" href="controller?command=viewMedCard&id=${patient.id}&currentPage=${i}&desc=${desc}">${i}</a></li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					<c:choose>
						<c:when test="${currentPage eq countPage}">
							<li class="page-item disabled">
								<a class="page-link" href="#" tabindex="-1" aria-disabled="true">Next</a>
							</li>
						</c:when>
						<c:otherwise>
							<li class="page-item">
								<a class="page-link" href="controller?command=viewMedCard&id=${patient.id}&currentPage=${currentPage+1}&desc=${desc}">Next</a>
							</li>
						</c:otherwise>
					</c:choose>
				</ul>
			</nav>
	</div>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>