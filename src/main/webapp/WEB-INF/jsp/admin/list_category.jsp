<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="List category" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="container-fluid">
	<div class="row flex-xl-nowrap">
		<%@ include file="/WEB-INF/jspf/menu.jspf" %>
		<div class="container p-3 mx-auto">
			<table class=".table-borderless mx-auto">
				<tr>
					<td>
<%--						<form class="form-inline float-left" action="controller?command=findCategory">--%>
<%--							<input class="form-control mr-sm-2" type="text" placeholder="<fmt:message key="list_jsp.table.enter_name"/>">--%>
<%--							<button class="btn btn-primary" type="submit"><fmt:message key="list_jsp.table.search"/></button>--%>
<%--						</form>--%>
						<form class="form-inline float-right" action="controller?command=openCategoryForm" method="post">
							<button class="btn btn-primary float-right" type="submit"><fmt:message key="jsp.button.add..."/></button>
						</form>
					</td>
				</tr>
				<tr>
					<td>
						<table class="table .table-sm table-striped table-responsive">
							<thead>
							<tr>
								<td>№</td>
								<td><c:choose>
									<c:when test="${!desc}">
										<a href="controller?command=listCategory&desc=true">
											<fmt:message key="list_personal_jsp.table.header.category_asc"/>
										</a>
									</c:when>
									<c:otherwise>
										<a href="controller?command=listCategory">
											<fmt:message key="list_personal_jsp.table.header.category_desc"/>
										</a>
									</c:otherwise>
								</c:choose></td>
								<td><fmt:message key="list_personal_jsp.table.header.amount"/></td>
								<td><fmt:message key="list_personal_jsp.table.header.option"/></td>
							</tr>
							</thead>
							<c:set var="i" value="0" scope="page"/>
							<c:forEach var="bean" items="${categoryList}">
								<tr>
									<td>${i=i+1}</td>
									<my:category categoryId="${bean.id}">
										<td>${pageContext.getAttribute("value")}</td>
									</my:category>
									<my:count categoryId="${bean.id}">
										<td class="text-center">${pageContext.getAttribute("countPersonal")}</td>
									</my:count>
									<td>
										<a href="controller?command=openEditCategoryForm&id=${bean.id}">
											<fmt:message key="list_jsp.table.edit"/>
										</a>
										<a class="ml-3" href="controller?command=delCategory&id=${bean.id}">
											<fmt:message key="list_jsp.table.delete"/>
										</a>
									</td>
								</tr>
							</c:forEach>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<nav aria-label="Page navigation example">
							<ul class="pagination justify-content-start">
								<c:choose>
									<c:when test="${currentPage eq 1}">
										<li class="page-item disabled">
											<a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
										</li>
									</c:when>
									<c:otherwise>
										<li class="page-item">
											<a class="page-link" href="controller?command=listCategory&currentPage=${currentPage-1}">Previous</a>
										</li>
									</c:otherwise>
								</c:choose>
								<c:forEach var="i" begin="1" end="${countPage}">
									<c:choose>
										<c:when test="${currentPage eq i}">
											<li class="page-item active"><a class="page-link" href="controller?command=listCategory&currentPage=${i}">${i}</a></li>
										</c:when>
										<c:otherwise>
											<li class="page-item"><a class="page-link" href="controller?command=listCategory&currentPage=${i}">${i}</a></li>
										</c:otherwise>
									</c:choose>
								</c:forEach>
								<c:choose>
									<c:when test="${currentPage eq countPage}">
										<li class="page-item disabled">
											<a class="page-link" href="#" tabindex="-1" aria-disabled="true">Next</a>
										</li>
									</c:when>
									<c:otherwise>
										<li class="page-item">
											<a class="page-link" href="controller?command=listCategory&currentPage=${currentPage+1}">Next</a>
										</li>
									</c:otherwise>
								</c:choose>
							</ul>
						</nav>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>