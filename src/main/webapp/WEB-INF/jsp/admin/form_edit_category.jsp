<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="Form personal" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="container-fluid">
	<div class="row flex-xl-nowrap">
		<%@ include file="/WEB-INF/jspf/menu.jspf" %>
		<div class="container p-3 mx-auto">
			<form action="controller" method="post">
				<input type="hidden" name="command" value="editCategory"/>
				<input type="hidden" name="id" value="${category.id}"/>
				<div class="form-row">
					<div class="col-md-10 mb-3">
						<p class="text-danger">${errorMessage}</p>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6 mb-3">
						<label for="validationDefault01"><fmt:message key="form_new_category_jsp.name_ua"/></label>
						<input type="text" class="form-control" name="nameUa" id="validationDefault01" value="${category.nameUa}" required>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6 mb-3">
						<label for="validationDefault02"><fmt:message key="form_new_category_jsp.name_en"/></label>
						<input type="text" class="form-control" name="nameEn" id="validationDefault02" value="${category.nameEn}" required>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6 mb-3">
						<label for="validationDefault03"><fmt:message key="form_new_category_jsp.name_ru"/></label>
						<input type="text" class="form-control" name="nameRu" id="validationDefault03" value="${category.nameRu}" required>
					</div>
				</div>
				<button class="btn btn-primary" type="submit"><fmt:message key="settings_jsp.button.update"/></button>
			</form>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>