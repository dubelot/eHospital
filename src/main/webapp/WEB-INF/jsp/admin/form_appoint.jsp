<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="Form appoint" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="container-fluid">
	<div class="row flex-xl-nowrap">
		<%@ include file="/WEB-INF/jspf/menu.jspf" %>
		<div class="container p-3 mx-auto">
			<div class="form-row">
				<p class="font-weight-bold pl-3"><fmt:message key="menu_jspf.anchor.patient"/>:	</p>
				<a class="font-italic text-dark pl-3" href="#" >${patient.firstName} ${patient.lastName}</a>
			</div>
			<div class="form-row">
				<p class="font-weight-bold pl-3"><fmt:message key="form_user_jsp.id_number"/>:	</p>
				<a class="font-italic text-dark pl-3" href="#" >${patient.idNumber}</a>
			</div>
			<div class="form-row">
				<p class="font-weight-bold pl-3"><fmt:message key="form_user_jsp.date_of_birth"/>:	</p>
				<a class="font-italic text-dark pl-3" href="#" >${patient.birthday}</a>
			</div>
			<c:if test="${patient.application ne null}">
				<div class="form-row">
					<p class="font-weight-bold pl-3"><fmt:message key="form_user_jsp.application_text"/>:	</p>
					<a class="font-italic text-dark pl-3" href="#">${patient.application}</a>
				</div>
			</c:if>
			<div class="form-row">
				<div class="col-md-6 mb-3">
					<form class="form-inline " action="controller" method="post">
						<input type="hidden" name="command" value="formAppointDoctor" />
						<input type="hidden" name="id" value="${patient.id}" />
						<label class="p-3" for="selectCat"><fmt:message key="form_user_jsp.category"/></label>
						<select class="custom-select pr-3" name="categoryId" id="selectCat" onchange="this.form.submit()">
						<c:choose>
							<c:when test="${not empty categoryId}">
							<option selected disabled value="${categoryId}}">
								<my:category categoryId="${categoryId}">
									${pageContext.getAttribute("value")}
								</my:category>
							</option>
							</c:when>
							<c:otherwise>
								<option selected disabled value=""><fmt:message key="form_user_jsp.choose"/></option>
							</c:otherwise>
						</c:choose>
						<c:forEach var="bean" items="${categoryList}">
							<my:category categoryId="${bean.id}">
								<option value="${bean.id}">${pageContext.getAttribute("value")}</option>
							</my:category>
						</c:forEach>
					</select>
					</form>
				</div>
			</div>
			<div class="form-row">
				<h4 class="font-weight-bold"><fmt:message key="form_appoint_jsp.appointed_doctors"/>:</h4>
			</div>
			<div class="form-row">
				<table class="table .table-sm table-striped table-responsive">
					<thead>
					<tr>
						<td>№</td>
						<td><fmt:message key="list_personal_jsp.table.header.personal"/></td>
						<td><fmt:message key="list_personal_jsp.table.header.id_number"/></td>
						<td><fmt:message key="list_personal_jsp.table.header.category"/></td>
						<td><fmt:message key="list_personal_jsp.table.header.option"/></td>
					</tr>
					</thead>
					<c:set var="i" value="${0}" scope="page"/>
					<c:forEach var="bean" items="${personalList}">
						<tr>
							<td>${i=i+1}</td>
							<td>${bean.lastName} ${bean.firstName}</td>
							<td>${bean.idNumber}</td>
							<my:category categoryId="${bean.categoryId}">
								<td>${pageContext.getAttribute("value")}</td>
							</my:category>
							<td>
								<form class="form-inline " action="controller" method="post">
									<input type="hidden" name="command" value="detachDoctor" />
									<input type="hidden" name="patientId" value="${patient.id}"/>
									<input type="hidden" name="personalId" value="${bean.id}">
									<button class="btn-primary" type="submit"><fmt:message key="form_appoint_jsp.detach"/></button>
								</form>
							</td>
						</tr>
					</c:forEach>
				</table>
			</div>
			<c:if test="${not empty doctorByCategoryList}">
				<div class="form-row">
					<p class="font-weight-bold"><fmt:message key="form_user_jsp.choose_doctor"/>:	</p>
				</div>
				<div class="form-row">
					<table class="table .table-sm table-striped table-responsive">
						<thead>
						<tr>
							<td>№</td>
							<td><fmt:message key="list_personal_jsp.table.header.personal"/></td>
							<td><fmt:message key="list_personal_jsp.table.header.id_number"/></td>
							<td><fmt:message key="list_personal_jsp.table.header.category"/></td>
							<td><fmt:message key="list_personal_jsp.table.header.amountPatient"/></td>
							<td><fmt:message key="list_personal_jsp.table.header.option"/></td>
						</tr>
						</thead>
						<c:set var="i" value="${0}" scope="page"/>
						<c:forEach var="bean" items="${doctorByCategoryList}">
							<tr>
								<td>${i=i+1}</td>
								<td>${bean.lastName} ${bean.firstName}</td>
								<td>${bean.idNumber}</td>
								<my:category categoryId="${bean.categoryId}">
									<td>${pageContext.getAttribute("value")}</td>
								</my:category>
								<td class="text-center">${bean.countPatient}</td>
								<td>
									<form class="form-inline " action="controller" method="post">
										<input type="hidden" name="command" value="appointDoctor" />
										<input type="hidden" name="patientId" value="${patient.id}"/>
										<input type="hidden" name="personalId" value="${bean.id}">
										<button class="btn-primary" type="submit"><fmt:message key="form_appoint_jsp.appoint"/></button>
									</form>
								</td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</c:if>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>