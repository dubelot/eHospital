<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="Form personal" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="container-fluid">
	<div class="row flex-xl-nowrap">
		<%@ include file="/WEB-INF/jspf/menu.jspf" %>
		<div class="container p-3 mx-auto">
			<form action="controller" method="post">
				<input type="hidden" name="command" value="addCategory"/>
				<div class="form-row">
					<div class="col-md-10 mb-3">
						<p class="text-danger">${errorMessage}</p>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6 mb-3">
						<label for="validationDefault01"><fmt:message key="form_new_category_jsp.name_ua"/></label>
						<c:choose>
							<c:when test="${errorMessage ne null}">
								<input type="text" class="form-control" name="nameUa" id="validationDefault01" value="${nameUa}" required>
							</c:when>
							<c:otherwise>
								<input type="text" class="form-control" name="nameUa" id="validationDefault01" placeholder="<fmt:message key="form_new_category_jsp.enter_name_ua"/>" required>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6 mb-3">
						<label for="validationDefault02"><fmt:message key="form_new_category_jsp.name_en"/></label>
						<c:choose>
							<c:when test="${errorMessage ne null}">
								<input type="text" class="form-control" name="nameEn" id="validationDefault02" value="${nameEn}" required>
							</c:when>
							<c:otherwise>
								<input type="text" class="form-control" name="nameEn" id="validationDefault02" placeholder="<fmt:message key="form_new_category_jsp.enter_name_en"/>" required>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-6 mb-3">
						<label for="validationDefault03"><fmt:message key="form_new_category_jsp.name_ru"/></label>
						<c:choose>
							<c:when test="${errorMessage ne null}">
								<input type="text" class="form-control" name="nameRu" id="validationDefault03" value="${nameRu}" required>
							</c:when>
							<c:otherwise>
								<input type="text" class="form-control" name="nameRu" id="validationDefault03" placeholder="<fmt:message key="form_new_category_jsp.enter_name_ru"/>" required>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<button class="btn btn-primary" type="submit"><fmt:message key="form_user_jsp.add"/></button>
			</form>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>