<%@ page pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<html>
<c:set var="title" value="List personal" scope="page" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="container-fluid">
	<div class="row flex-xl-nowrap">
		<%@ include file="/WEB-INF/jspf/menu.jspf" %>
		<div class="container p-3 mx-auto">
			<table class=".table-borderless mx-auto">
				<tr>
					<td>
<%--						<form class="form-inline float-left" action="controller?command=findUser">--%>
<%--							<input class="form-control mr-sm-2" type="text" placeholder="<fmt:message key="list_jsp.table.enter_name"/>">--%>
<%--							<button class="btn btn-primary" type="submit"><fmt:message key="list_jsp.table.search"/></button>--%>
<%--						</form>--%>
						<c:choose>
							<c:when test="${orderBy=='count' and !desc}">
								<a class="ml-3 my-3 float-right" href="controller?command=listPersonal&orderBy=count&desc=true">
									<fmt:message key="list_personal_jsp.table.by_count_desc"/>
								</a>
							</c:when>
							<c:when test="${orderBy=='count' and desc}">
								<a class="ml-3 my-3 float-right" href="controller?command=listPersonal&orderBy=count&desc=false">
									<fmt:message key="list_personal_jsp.table.by_count_asc"/>
								</a>
							</c:when>
							<c:otherwise>
								<a class="ml-3 my-3 float-right" href="controller?command=listPersonal&orderBy=count">
									<fmt:message key="list_personal_jsp.table.by_count"/>
								</a>
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${orderBy=='category' and desc}">
								<a class="ml-3 my-3 float-right" href="controller?command=listPersonal&orderBy=category&desc=false">
									<fmt:message key="list_personal_jsp.table.by_category_desc"/>
								</a>
							</c:when>
							<c:when test="${orderBy=='category' and !desc}">
								<a class="ml-3 my-3 float-right" href="controller?command=listPersonal&orderBy=category&desc=true">
									<fmt:message key="list_personal_jsp.table.by_category_asc"/>
								</a>
							</c:when>
							<c:otherwise>
								<a class="ml-3 my-3 float-right" href="controller?command=listPersonal&orderBy=category">
									<fmt:message key="list_personal_jsp.table.by_category"/>
								</a>
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${orderBy == 'name' and desc}">
								<a class="ml-3 my-3 float-right" href="controller?command=listPersonal&orderBy=name&desc=false">
									<fmt:message key="list_personal_jsp.table.by_name_desc"/>
								</a>
							</c:when>
							<c:when test="${orderBy == 'name' and !desc}">
								<a class="ml-3 my-3 float-right" href="controller?command=listPersonal&orderBy=name&desc=true">
									<fmt:message key="list_personal_jsp.table.by_name_asc"/>
								</a>
							</c:when>
							<c:otherwise>
								<a class="ml-3 my-3 float-right" href="controller?command=listPersonal&orderBy=name">
									<fmt:message key="list_personal_jsp.table.by_name"/>
								</a>
							</c:otherwise>
						</c:choose>

						<p class="ml-3 my-3 float-right"><fmt:message key="list_personal_jsp.table.header.sort"/></p>
					</td>
				</tr>
				<tr>
					<td>
						<table class="table .table-sm table-striped table-responsive">
							<thead>
								<tr>
									<td>№</td>
									<td><fmt:message key="list_personal_jsp.table.header.personal"/></td>
									<td><fmt:message key="list_personal_jsp.table.header.id_number"/></td>
									<td><fmt:message key="list_personal_jsp.table.header.category"/></td>
									<td><fmt:message key="list_personal_jsp.table.header.status"/></td>
									<td><fmt:message key="list_personal_jsp.table.header.amountPatient"/></td>
									<td><fmt:message key="jsp_entity_type.nurse"/></td>
									<td><fmt:message key="list_personal_jsp.table.header.option"/></td>
								</tr>
							</thead>
							<c:set var="i" value="${currentPage*10-10}" scope="page"/>
							<c:forEach var="bean" items="${personalList}">

							<tr>
								<td>${i=i+1}</td>
								<td>${bean.lastName} ${bean.firstName}</td>
								<td>${bean.idNumber}</td>
								<my:category categoryId="${bean.categoryId}">
									<td>${value}</td>
								</my:category>
								<c:if test="${bean.roleId=='1'}">
									<td><fmt:message key="jsp_entity_type.doctor"/></td>
								</c:if>
								<c:if test="${bean.roleId=='2'}">
								<td><fmt:message key="jsp_entity_type.nurse"/></td>
								</c:if>
								<td class="text-center">${bean.countPatient}</td>
								<td><my:personal personalId="${bean.nurseId}">${personal.firstName.charAt(0)}.${personal.lastName}</my:personal>
								</td>
								<td>
									<a href="controller?command=openEditUserForm&editType=personal&id=${bean.id}">
										<fmt:message key="list_jsp.table.edit"/>
									</a>
									<a class="mb-3 mb-md-0 ml-md-3" href="controller?command=delUser&delType=personal&id=${bean.id}">
										<fmt:message key="list_jsp.table.delete"/>
									</a>
								</td>
							</tr>
							</c:forEach>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<nav aria-label="Page navigation example">
							<ul class="pagination justify-content-start">
								<c:choose>
									<c:when test="${currentPage eq 1}">
										<li class="page-item disabled">
											<a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
										</li>
									</c:when>
									<c:otherwise>
										<li class="page-item">
											<a class="page-link" href="controller?command=listPersonal&currentPage=${currentPage-1}&orderBy=${orderBy}&desc=${desc}">Previous</a>
										</li>
									</c:otherwise>
								</c:choose>
								<c:forEach var="i" begin="1" end="${countPage}">
									<c:choose>
										<c:when test="${currentPage eq i}">
											<li class="page-item active"><a class="page-link" href="controller?command=listPersonal&currentPage=${i}&orderBy=${orderBy}&desc=${desc}">${i}</a></li>
										</c:when>
										<c:otherwise>
											<li class="page-item"><a class="page-link" href="controller?command=listPersonal&currentPage=${i}&orderBy=${orderBy}&desc=${desc}">${i}</a></li>
										</c:otherwise>
									</c:choose>
								</c:forEach>
								<c:choose>
								<c:when test="${currentPage eq countPage}">
									<li class="page-item disabled">
										<a class="page-link" href="#" tabindex="-1" aria-disabled="true">Next</a>
									</li>
								</c:when>
								<c:otherwise>
									<li class="page-item">
										<a class="page-link" href="controller?command=listPersonal&currentPage=${currentPage+1}&orderBy=${orderBy}&desc=${desc}">Next</a>
									</li>
								</c:otherwise>
								</c:choose>
							</ul>
						</nav>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
</body>
</html>