package com.epam.learn.odubelt.hospital;

import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.web.command.personal.AddCardEntryCommand;
import com.epam.learn.odubelt.hospital.web.command.personal.OpenEntryFormCommand;
import com.epam.learn.odubelt.hospital.web.command.personal.PersonalDefaultCommand;
import org.junit.jupiter.api.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PersonalCommandTester {
    HttpServletRequest request = mock(HttpServletRequest.class);
    HttpServletResponse response = mock(HttpServletResponse.class);
    @Test
    public void addCardEntryCommandTest() throws IOException, ServletException {
        assertEquals(Path.COMMAND_SUCCESS, new AddCardEntryCommand().execute(request, response));
    }
    @Test
    public void openEntryFormCommandTest() throws IOException, ServletException {
        assertEquals(Path.PAGE_ENTRY_TYPE_SELECT, new OpenEntryFormCommand().execute(request, response));
    }
    @Test
    public void personalDefaultCommandTest() throws IOException, ServletException {
        PersonalDefaultCommand c = mock(PersonalDefaultCommand.class);
        when(c.execute(request, response)).thenReturn(Path.PAGE_PERSONAL_DEFAULT);
        assertEquals(Path.PAGE_PERSONAL_DEFAULT, c.execute(request, response));
    }


}
