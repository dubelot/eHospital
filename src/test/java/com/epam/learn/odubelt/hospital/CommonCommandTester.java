package com.epam.learn.odubelt.hospital;
import com.epam.learn.odubelt.hospital.web.Path;
import com.epam.learn.odubelt.hospital.web.command.*;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Test;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

public class CommonCommandTester {
    HttpServletRequest request = mock(HttpServletRequest.class);
    HttpServletResponse response = mock(HttpServletResponse.class);
    Logger logger = Logger.getLogger(LocaleSetCommand.class);
    @Test
    public void commandTest() {
        assertEquals("NoCommand", new NoCommand().toString());
    }
    @Test
    public void commandContainerTest() {
        assertEquals(NoCommand.class, CommandContainer.get(null).getClass());
    }
    @Test
    public void commonCommandsTest() {
        assertEquals(1, CommonCommands.setPaginationParameters(request, 5, 5));
    }
    @Test
    public void localeSetCommandTest() throws IOException, ServletException {
        LocaleSetCommand c =new LocaleSetCommand();
        assertEquals("", c.execute(request, response));
    }
    @Test
    public void loginCommandTest() throws IOException, ServletException {
        LoginCommand c = mock(LoginCommand.class);
        when(c.execute(request, response)).thenReturn(Path.COMMAND_LIST_PERSONAL);
        assertEquals(Path.COMMAND_LIST_PERSONAL, c.execute(request, response));
    }
    @Test
    public void logoutCommandTest() throws IOException, ServletException {
        LogoutCommand c = mock(LogoutCommand.class);
        when(c.execute(request, response)).thenReturn(Path.PAGE_LOGIN);
        assertEquals(Path.PAGE_LOGIN, c.execute(request, response));
    }
    @Test
    public void noCommandTest() throws IOException, ServletException {
        NoCommand c = mock(NoCommand.class);
        when(c.execute(request, response)).thenReturn(Path.PAGE_ERROR_PAGE);
        assertEquals(Path.PAGE_ERROR_PAGE, c.execute(request, response));
    }
    @Test
    public void savePDFCommandTest() throws IOException, ServletException {
        SavePDFCommand c = mock(SavePDFCommand.class);
        when(c.execute(request, response)).thenReturn("Card_Entry1");
        assertEquals("Card_Entry1", c.execute(request, response));
    }
    @Test
    public void successCommandTest() throws IOException, ServletException {
        SuccessCommand vsc = new SuccessCommand();
        assertEquals(Path.PAGE_SUCCESS, vsc.execute(request, response));
    }
    @Test
    public void updateSettingsCommandTest() throws IOException, ServletException {
        UpdateSettingsCommand c = mock(UpdateSettingsCommand.class);
        when(c.execute(request, response)).thenReturn("/WEB-INF/jsp/settings.jsp");
        assertEquals(Path.PAGE_SETTINGS, c.execute(request, response));
    }
    @Test
    public void viewMedCardCommandTest() throws IOException, ServletException {
        ViewMedCardCommand c = mock(ViewMedCardCommand.class);
        when(c.execute(request, response)).thenReturn("/WEB-INF/jsp/med_card.jsp");
        assertEquals(Path.PAGE_MED_CARD, c.execute(request, response));
    }

    @Test
    public void viewSettingsCommandTest() throws IOException, ServletException {
        ViewSettingsCommand vsc = new ViewSettingsCommand();
        assertEquals(Path.PAGE_SETTINGS, vsc.execute(request, response));
    }

}
