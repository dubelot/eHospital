package com.epam.learn.odubelt.hospital;

import com.epam.learn.odubelt.hospital.web.EHospUtility;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class PasswordHashingTest {
    @Test
    public void passwordHash(){
        String password = "password";
        String hashed = EHospUtility.hashPassword(password);
        String hashed2 = EHospUtility.hashPassword(password);
        assertNotEquals(hashed, hashed2);
        assertTrue(EHospUtility.comparePasswords(password, hashed));
        assertTrue(EHospUtility.comparePasswords(password, hashed2));
    }
}
